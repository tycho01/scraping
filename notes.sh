# vagrant
vagrant up
vagrant ssh
exit
# vagrant halt
vagrant reload
vagrant ssh

# linux
# beep
echo -e '\a'
# beep python
print '\a'
# file search
sudo find -name file.ext
# hidden files
ls -a
# processes
ps aux
ps aux | grep cloudera
# kill
sudo kill -9 PID
# services
service --status-all
# ports
nmap -sT -O localhost
sudo lsof -i
sudo netstat -lptu
sudo netstat -tulpn
# install
sudo dpkg -i pkg.deb

# test GUIs
# apache
curl http://localhost:88
# rails
curl http://localhost:3000
# rethinkdb
# rethinkdb --daemon --bind all
curl http://localhost:8080
# neo4j
# sudo service neo4j-service start
curl http://localhost:7474
# flower
# cd /vagrant/taobao
# celery -A proj.settings flower
curl http://localhost:5555
# rabbitmq
# sudo rabbitmq-plugins enable rabbitmq_management
curl http://localhost:15672
# redis (sinatra)
# redis-server /vagrant/taobao/scrapy-redis/redis.conf
# redis-browser -B 0.0.0.0
curl http://localhost:4567
# redis (node)
# redis-server /vagrant/taobao/scrapy-redis/redis.conf
# # cd ~
# # redis-commander
# curl http://localhost:8081
# cloudera
# sudo service cloudera-scm-server start
# urls FAIL
curl http://localhost:7180
# cloudera quickstart
curl http://localhost:18080
# Hue
curl http://localhost:8888
# Rodeo
# rodeo . --host=0.0.0.0
curl http://localhost:5000
# # spark notebook
# # sudo docker run -p 9000:9000 andypetrella/spark-notebook:0.4.2-scala-2.11.2-spark-1.3.0-hadoop-2.6.0
# curl http://localhost:9000


# cloudera
sudo apt-get install -y ant gcc g++ libkrb5-dev libmysqlclient-dev libssl-dev libsasl2-dev libsasl2-modules-gssapi-mit libsqlite3-dev libxml2-dev libxslt-dev maven libldap2-dev python-dev python-simplejson python-setuptools
https://www.cloudera.com/content/cloudera/en/downloads/cloudera_manager/cm-5-4-0.html
sudo ./cloudera-manager-installer.bin
http://localhost:7180/
admin/admin

# rodeo
sudo pip install rodeo # virtualenv gunicorn

# neo4j
wget -O - http://debian.neo4j.org/neotechnology.gpg.key >> key.pgp
sudo apt-key add key.pgp
echo 'deb http://debian.neo4j.org/repo stable/' | sudo tee -a /etc/apt/sources.list.d/neo4j.list > /dev/null
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer
sudo apt-get install neo4j
sudo vi /etc/neo4j/neo4j-server.properties
org.neo4j.server.webserver.address=0.0.0.0
sudo service neo4j-service start
http://localhost:7474/
# JS error in Canary, others work
neo4j/qwerty

# postgresql
git clone https://bitbucket.org/openscg/pgstudio.git
cd pgstudio
sudo apt-get install -y ant
ant clean
ant deploy

# rethinkdb
cd ~
rethinkdb --daemon --bind all --http-port 8080
pkill -9 rethinkdb
curl http://localhost:8080/
http://rethinkdb.com/api/javascript/

# spark
# sudo docker pull andypetrella/spark-notebook:0.4.2-scala-2.11.2-spark-1.3.0-hadoop-2.6.0
# sudo docker run -p 9000:9000 andypetrella/spark-notebook:0.4.2-scala-2.11.2-spark-1.3.0-hadoop-2.6.0
# curl http://localhost:9000/
sudo docker pull ypandit/hdp-spark
sudo docker run -td ypandit/hdp-spark

# scrapy
cd /vagrant/taobao/scrapy-redis/example-project/taobao/spiders
scrapy crawl tb

# redis
redis-server /vagrant/taobao/scrapy-redis/redis.conf &
redis-cli
gem install redis-browser
redis-browser -B 0.0.0.0
curl http://localhost:4567/
# npm install -g redis-commander
# cd ~
# redis-commander
# curl http://localhost:8081/
# webdis
git clone git://github.com/nicolasff/webdis.git && cd webdis && make
~/webdis/webdis &
curl localhost:7379/GET/y

# rabbitmq
sudo rabbitmqctl list_queues
sudo rabbitmq-plugins enable rabbitmq_management
http://localhost:15672/
sudo rabbitmqctl add_user test test
sudo rabbitmqctl set_user_tags test administrator
sudo rabbitmqctl set_permissions -p / test ".*" ".*" ".*"

# supervisor
cat /etc/supervisord.conf
cp /etc/supervisord.conf /vagrant
sudo cp /vagrant/supervisord.conf /etc
supervisord -c /etc/supervisord.conf
# supervisorctl -c /etc/supervisord.conf reread
supervisorctl -c /etc/supervisord.conf update

# celery
cd /vagrant/taobao/proj
celery -A settings worker -l info -P gevent -c 1 -f celery.log
celery multi start w1 -A settings -l info -P gevent -c 1 -f w1.log
celery multi stopwait w1 -A settings -l info
celery multi start 10 -A settings -l info -P gevent --pidfile=/var/run/celery/%n.pid -f /var/log/celery/%n%I.log -Q:1-3 images,video -Q:4,5 data -Q default -L:4,5 debug
# http://docs.celeryproject.org/en/latest/reference/celery.bin.multi.html#module-celery.bin.multi
python run.py

# flower
cd /vagrant/taobao/proj
celery -A settings flower
curl http://localhost:5555/
# curl -X POST -d '{"args":[3,4]}' http://localhost:5555/api/task/send-task/tasks.add
curl -X POST -d '{"args":["http://www.reddit.com/r/asoiaf/"]}' http://localhost:5555/api/task/send-task/tasks.urlopen

# java
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
cd /vagrant
mv jdk-8u45-linux-x64.gz /opt/jdk/
cd /opt/jdk
sudo tar zxvf jdk-8u45-linux-x64.gz
rm jdk-8u45-linux-x64.gz
sudo update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_45/bin/java 1081
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk/jdk1.8.0_45/bin/javac 110
sudo update-alternatives --display java
sudo update-alternatives --display javac
java -version

# docker
docker ps
docker run -itP user/image
docker stop $(docker ps -a -q)

# kubernetes (docker)
sudo apt-get install links
sudo vi /etc/default/docker
DOCKER_OPTS="$DOCKER_OPTS --insecure-registry=gcr.io"
sudo service docker restart
wget https://storage.googleapis.com/kubernetes-release/release/v0.17.0/bin/linux/amd64/kubectl
PATH=$PATH:/vagrant/kubernetes/
PATH=$PATH:/shared/kubernetes/
docker run --net=host -d gcr.io/google_containers/etcd:2.0.9 /usr/local/bin/etcd --addr=127.0.0.1:4001 --bind-addr=0.0.0.0:4001 --data-dir=/var/etcd/data
docker run --net=host -d -v /var/run/docker.sock:/var/run/docker.sock  gcr.io/google_containers/hyperkube:v0.17.0 /hyperkube kubelet --api_servers=http://localhost:8080 --v=2 --address=0.0.0.0 --enable_server --hostname_override=127.0.0.1 --config=/etc/kubernetes/manifests
docker run -d --net=host --privileged gcr.io/google_containers/hyperkube:v0.17.0 /hyperkube proxy --master=http://127.0.0.1:8080 --v=2
kubectl get nodes

# kafka
# http://wurstmeister.github.io/kafka-docker/
docker-compose -f docker-compose-single-broker.yml up -d --no-recreate
docker-compose ps
# docker-compose scale kafka=n
# start-kafka-shell.sh <DOCKER_HOST_IP> <ZK_HOST:ZK_PORT>
./start-kafka-shell.sh localhost localhost:2181 # 9092
# try stuff in shell
$KAFKA_HOME/bin/kafka-topics.sh --create --topic topic --partitions 4 --zookeeper $ZK --replication-factor 2
$KAFKA_HOME/bin/kafka-topics.sh --describe --topic topic --zookeeper $ZK
$KAFKA_HOME/bin/kafka-console-producer.sh --topic=topic --broker-list=`broker-list.sh`
$KAFKA_HOME/bin/kafka-console-consumer.sh --topic=topic --zookeeper=$ZK

[dropwizard-kafka-http](https://github.com/stealthly/dropwizard-kafka-http)
# mondary/dropwizard -- no instructions, jarfile error

[HDP kafka CLI tutorial](http://jp.hortonworks.com/hadoop-tutorial/simulating-transporting-realtime-events-stream-apache-kafka/) (step 4)
[another](http://kafka.apache.org/07/quickstart.html)

cd /usr/hdp/2.2.4.2-2/kafka/
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic truckevent
bin/kafka-topics.sh --list --zookeeper localhost:2181


# hermes
# clone/unzip 0.7.1
cd ~/Desktop/hermes-hermes-0.7.1/docker/
# docker-compose.yml -> KAFKA_ADVERTISED_HOST_NAME: 172.17.42.1 # internal IP as found in ifconfig -> docker0
./build.sh
./run.sh
docker ps
# groups
curl -d '{"groupName": "com.example.events"}' -H "Content-Type: application/json" localhost:8080/groups
curl localhost:8080/groups
curl localhost:8080/groups/com.example.events
curl -X DELETE localhost:8080/groups/com.example.events
# topics
curl -d '{"name": "com.example.events.clicks"}' -H "Content-Type: application/json" localhost:8080/topics
curl localhost:8080/topics
curl localhost:8080/topics/com.example.events.clicks
curl -X DELETE localhost:8080/topics/com.example.events.clicks
# subscriptions
curl -d '{"name": "clicks-receiver", "endpoint": "http://requestb.in/1isy54g1"}' -H "Content-Type: application/json" localhost:8080/topics/com.example.events.clicks/subscriptions
curl localhost:8080/topics/com.example.events.clicks/subscriptions
curl localhost:8080/topics/com.example.events.clicks/subscriptions/clicks-receiver
curl -X DELETE localhost:8080/topics/com.example.events.clicks/subscriptions/clicks-receiver
# publish
curl -v -d '{"id": 12345, "page": "main"}' localhost:8888/topics/com.example.events.clicks
# working! :)

# k8s spark
./kubectl create -f ./examples/spark/spark-master.json
./kubectl create -f ./examples/spark/spark-master-service.json
./kubectl get pods,services
./kubectl create -f ./examples/spark/spark-worker-controller.json
SERVICE="spark-master"
IP=$(./kubectl get services | grep $SERVICE | grep -o '\([0-9]\+\.\)\{3\}[0-9]\+')
echo $IP
sudo docker run -it mattf/spark-base sh
# substitute $IP
echo "10.254.125.166 spark-master" >> /etc/hosts
export SPARK_LOCAL_HOSTNAME=$(hostname -i)
MASTER=spark://spark-master:7077 pyspark
# warning but sorta works

docker run -t -i ogirardot/spark-docker-shell:1.3.1
# works! :D

# zeppelin

# epahomov/docker-zeppelin
docker pull epahomov/docker-zeppelin
docker run -it -p 8080:8080 -p 8081:8081 epahomov/docker-zeppelin /bin/bash
cd incubator-zeppelin/
mvn clean package -Pspark-1.3 -Dhadoop.version=2.4.0 -Phadoop-2.4 -Pyarn -DskipTests
/incubator-zeppelin/bin/zeppelin-daemon.sh start
curl localhost:8080 # 8081
# interpreters not working

# dylanmei/zeppelin -- switches between interpreter not recognized and scheduler already terminated

# internavenue/centos-zeppelin
sudo docker run -t -i -p 8080:8080 -p 8081:8081 internavenue/centos-zeppelin:centos7
# localhost / docker ps -> nothing...

http://hortonworks.com/blog/introduction-to-data-science-with-apache-spark/
# maven
export M_HOME=/usr/local/apache-maven
mkdir $M_HOME
cd $M_HOME
wget http://mirror.olnevhost.net/pub/apache/maven/binaries/apache-maven-3.2.2-bin.tar.gz
tar xvf apache-maven-3.2.2-bin.tar.gz
export M2_HOME=$M_HOME/apache-maven-3.2.2
export M2=$M2_HOME/bin
export PATH=$M2:$PATH
mvn -version
# incubator-zeppelin
git clone https://github.com/apache/incubator-zeppelin.git
cd incubator-zeppelin
mvn clean install -DskipTests -Pspark-1.3 -Dspark.version=1.3.1 -Phadoop-2.6 -Pyarn
set hdp.version= hdp-select status hadoop-client | sed 's/hadoop-client - \(.*\)/\1/'
sudo cat conf/zeppelin-env.sh
```
export HADOOP_CONF_DIR=/etc/hadoop/conf
export ZEPPELIN_PORT=10008
export ZEPPELIN_JAVA_OPTS="-Dhdp.version=$hdp.version"
```
cp /etc/hive/conf/hive-site.xml conf/
su hdfs
hdfs dfs -mkdir /user/zeppelin;hdfs dfs -chown zeppelin:hdfs /user/zeppelin>
bin/zeppelin-daemon.sh start
curl localhost:10008
# error: mvn not found

# spark notebook
https://github.com/andypetrella/spark-notebook
docker pull andypetrella/spark-notebook:master-scala-2.11.2-spark-1.3.1-hadoop-2.6.0
docker run -p 9000:9000 andypetrella/spark-notebook:master-scala-2.11.2-spark-1.3.1-hadoop-2.6.0
# custom builds not available as Docker, try the .deb
sudo dpkg -i spark-notebook_master-scala-2.11.2-spark-1.3.1-hadoop-2.7.0_all.deb
sudo spark-notebook
localhost:9000
# Python Notebook gives errors :(

https://github.com/spark-jobserver/spark-jobserver
https://github.com/omriiluz/spark-jobserver
https://registry.hub.docker.com/u/omriiluz/spark-jobserver/
docker pull omriiluz/spark-jobserver
docker run -itP omriiluz/spark-jobserver
# errors, many docs but none starting from start :(

https://github.com/hohonuuli/sparknotebook
# worked for [him](http://iamtrask.github.io/2014/11/22/spark-gpu/) but iScala bash errors for me

https://github.com/tribbloid/ISpark
http://blog.cloudera.com/blog/2014/08/how-to-use-ipython-notebook-with-apache-spark/
https://registry.hub.docker.com/u/cineca/spark-ipython/
docker run -d -p 8888:8888 -p 8080:8080 -v /shared/notebooks --name pyspark cineca/spark-ipython
# Python Notebooks on localhost:8888 work! :D

# A lambda architecture based on kafka flume storm/trident hadoop and druid
https://registry.hub.docker.com/u/certxg/lambda/
docker run -itP certxg/lambda
# no docs so stuck :(

# Flink
https://registry.hub.docker.com/u/devmapal/flink/
docker pull devmapal/flink:0.8.1
docker run -itP devmapal/flink:0.8.1
# no docs so stuck :(
