r.createDb('scrape');
r.db('scrape').createTable('domains');
r.db('scrape').createTable('urls');
r.db('scrape').table('domains').indexCreate('domain');
r.db('scrape').table('domains').insert([{
  'domain':'taobao.com',
  'hourly_limit':5000,
  'hourly_tally':0,
  'headers':{'Referer':'http://item.taobao.com/','Cookie':'cookie2=81af83c96fe415ad48868c56f481189a'}
}, {
  'domain':'tmall.com',
  'hourly_limit':5000,
  'hourly_tally':0,
  'headers':{'Referer':'http://item.tmall.com/','Cookie':'cookie2=81af83c96fe415ad48868c56f481189a'}
}]);
r.db('scrape').table('urls').insert([{
  'url':'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=1',
  'api_key':null,
  'domain_id':1
}, {
  'url':'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=2',
  'api_key':null,
  'domain_id':1
}, {
  'url':'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=3',
  'api_key':null,
  'domain_id':1
}], {durability: "hard", returnChanges: false, conflict: "error"});
