DROP TABLE "urls";
CREATE TABLE "urls" (
	"id"	SERIAL,
	"url"	TEXT,
	"api_key"	TEXT,
	"domain_id"	INTEGER
);

INSERT INTO "urls" VALUES (1,'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=1',NULL,1);
INSERT INTO "urls" VALUES (2,'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=2',NULL,1);
INSERT INTO "urls" VALUES (3,'http://spu.taobao.com/spu/spulist.htm?cat=1512&page=3',NULL,1);

DROP TABLE "domains";
CREATE TABLE "domains" (
	"id"	SERIAL,
	"domain"	TEXT,
	"hourly_limit"	INTEGER,
	"hourly_tally"	INTEGER,
	"headers"	TEXT
);

INSERT INTO "domains" VALUES (1,'taobao.com',5000,0,' -H "Referer: http://item.taobao.com/" -H "Cookie: cookie2=81af83c96fe415ad48868c56f481189a"');

COMMIT;
