-- query URL to scrape per distinct domain [+ key combination]
WITH tmp
  AS ( 
  -- maybe just whatever I wanted, though including the group columns
  SELECT *,
   Row_number()
   OVER (
     partition BY
     -- group columns
     domain_id, api_key
   ) AS rn
  FROM urls)
SELECT
-- whatever I wanted
tmp.url,
tmp.api_key,
domains.headers
FROM   tmp
       JOIN domains
         ON tmp.domain_id = domains.id
WHERE  rn = 1
       AND domains.hourly_tally < domains.hourly_limit *
--	   (date +%M) * 60 + (date +%S) / 3600
	   ((date +%s) % 3600)
;
