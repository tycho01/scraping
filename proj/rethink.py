import rethinkdb as r
import json
# import unirest
# from requests import async
from requests_futures.sessions import FuturesSession

def type_of_change(change):
    '''Determines whether the change is a create, delete or update'''
    if change['old_val'] is None:
        return 'create'
    elif change['new_val'] is None:
        return 'delete'
    else:
        return 'update'

def handle_flower(sess, resp):
    if resp.code > 200:
        raise Exception(str(resp))

def handle(url):
    print url
    flower_url = "http://localhost:5555/api/task/send-task/" + task
    data = json.dumps({'args':[url['url']]})
    # headers = url['headers']
    headers = {}
    # async.post(flower_url, data=data, headers=headers, callback=handle_flower)
    # unirest.post(flower_url, params=data, headers=headers, callback=callback_function)
    session = FuturesSession()
    session.post(flower_url, data=data, headers=headers, background_callback=handle_flower)

    # async? batch outside loop? enable durability without sync?
    (r.table('domains').get(url['domain_id'])
        .update({'hourly_tally': r.row['hourly_tally'] + 1 }).default(1)
        .run(rethink_conn, durability='soft')
    )
    r.table('urls').get(url['id']).delete().run(rethink_conn)
    # persist durability:soft writes in batch
    # r.table('domains').sync().run(rethink_conn)

task = 'tasks.urlopen'
rethink_conn = r.connect(host='localhost', port=28015, db='scrape', auth_key="", timeout=20)

urls = (
    r.table('domains')
    .order_by(r.desc(lambda doc: doc['hourly_tally'] / doc['hourly_limit'] ))   # Cannot call `changes` on an eager stream
    # ^ must be used with indexed column instead of lambda, which sucks as it'd require rewriting updating all documents every second or more?
    .limit(1)
    .merge(lambda domain: {
    	'available': r.table('urls').filter({'domain_id': domain['id']}).count(),
        'due': r.expr(domain['hourly_limit']).mul(
                r.now().time_of_day().do(r.js("Math.floor")).mod(3600)
        		).div(3600).do(r.js("Math.floor")).sub(domain['hourly_tally'])
    })
    .merge(lambda domain: {
    	'ready': r.expr([domain['available'], domain['due']]).min()
    })
    .do(lambda domain:
        r.table('urls')
	    # .eq_join('domain_id', domain, index='id').zip()    # Cannot call `changes` after `concat_map`
        # .limit(domain.sum('ready'))
        .order_by(index='id') # solves error
        .filter({'domain_id': domain['id']})
        .limit(domain.sum('ready'))   # sum of 1 entry.. how else?
        .merge({'headers': domain['headers']})
    )
)

# r.db('scrape').table('domains')
#   .orderBy(function(doc) {
#     return doc('hourly_tally').div(doc('hourly_limit'))
# })
# .limit(1)
# .merge(function(domain){
#     return {
# 		available: r.db('scrape').table('urls').filter({domain_id: domain('id')}).count(),
# 		due:
# 		r.expr(domain('hourly_limit')).mul(
# 		r.now().timeOfDay().do(r.js("Math.floor")).mod(3600)
# 		).div(3600).do(r.js("Math.floor")).sub(domain('hourly_tally'))
# 	};
# })
# .merge(function(domain){
#     return {
# 		ready: r.expr([domain('available'), domain('due')]).min()
# 	};
# })
# .do(function(domain){
# 	return r.db('scrape').table('urls')
# 	.eqJoin('domain_id', domain, {index:'id'}).zip()
#     .limit(domain.sum('ready'));
# });


result = urls.run(rethink_conn)
print result
changes = urls.changes().run(rethink_conn)
print changes

try:
    list = list(changes)
    print 'checking urls'
    for url in result:
        handle(url)
    print 'checking changes'
    for change in changes:
        kind = type_of_change(change)
        print "type: " + kind
        if kind == "create":
            url = change['new_val']
            handle(url)
except Exception as e:
    print e
except BaseException as e:
    print "You pressed Ctrl-C!"
finally:
    changes.close()
    rethink_conn.close()
