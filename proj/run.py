# -*- coding: utf-8 -*-
from tasks import urlopen, add, echo
from celery import group

print urlopen("http://www.reddit.com/r/asoiaf/")
result = urlopen.delay("http://www.reddit.com/r/asoiaf/")
print result.get(timeout=10)
# group(add.s(i, i) for i in xrange(10))().get()
# print add(1, 1)
# result = add.delay(4, 4)
# print result.ready()
# print result.id
# print result.get(timeout=1)
# result = echo.delay("HAHAAAAAAAAAA")
# print result.get(timeout=1)

# result = group(urlopen.s(url) for url in LIST_OF_URLS).apply_async()
# for incoming_result in result.iter_native():
# 	print(incoming_result, )
