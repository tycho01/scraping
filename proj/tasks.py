# -*- coding: utf-8 -*-
from __future__ import absolute_import
from settings import app
import sys
import inspect
import requests
import pika
import redis
# http://docs.celeryproject.org/en/latest/userguide/canvas.html#the-primitives
from celery import task, group, chain #, cord, map, starmap, chunks
# from pybloom import BloomFilter

@app.task
def echo(s):
    return s

@app.task
def add(x, y):
    return x + y

@app.task   #(ignore_result=True, serializer='pickle', compression='zlib')
def urlopen(url):
    print('Opening: {0}'.format(url))
    # with Timeout(5, False):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2392.0 Safari/537.36'}
        r = requests.get(url, headers=headers)
    # # define task-based max_retries, default_retry_delay
    # except SomeNetworkException as e:
    #     self.retry(e)
    except Exception as e:
        print('Exception for {0}: {1!r}'.format(url, e))
        return url, 0
    # subtasks = group(urlopen.s(url) for url in wanted_urls)
    # subtasks()
    # return url, 1
    # return len(response.text)
    if (r.status_code != 200):
        print('Status code for {0}: {1}'.format(url, r.status_code))
        return url, 0
    else:
        result = r.content.decode(r.encoding) #.encode('utf-8')

        # RabbitMQ
        func_name = inspect.currentframe().f_code.co_name
        key = "result." + func_name
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()
        channel.exchange_declare(exchange=key, type='direct')
        channel.queue_declare(queue=key, durable=True)
        channel.queue_bind(queue=key, exchange=key, routing_key=key)
        channel.basic_publish(exchange=key, routing_key=key, body=result, properties=pika.BasicProperties(delivery_mode = 2))
        channel.close()
        connection.close()
        # print "Stored to RabbitMQ exchange/queue/route " + key

        # Redis, SQS, Cassandra, ...
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.set(url, result)
        # print "Stored to Redis"
        # print r.get(url)

        return result
