# -*- coding: utf-8 -*-
# http://docs.celeryproject.org/en/latest/configuration.html#configuration
# syntax check: python -m celeryconfig

# import os
# import sys
# sys.path.insert(0, os.getcwd())
from __future__ import absolute_import
from kombu import Exchange, Queue
from celery import Celery
# app = Celery('proj',broker='amqp',include=['proj.tasks'])
app = Celery('proj',
    # backend='redis://localhost',
    # backend='amqp',
    # backend='amqp://',
    # broker='amqp://guest@localhost//'
    # broker='amqp://',
    broker='amqp',
    # include=['proj.tasks']
    include=['tasks']
)
# app.config_from_object('celeryconfig')

TASKS = ['add','echo','urlopen']

# Optional configuration, see the application user guide.
app.conf.update(
    BROKER_URL = 'amqp://guest:guest@localhost:5672//',
    # BROKER_CONNECTION_TIMEOUT = 4,
    # BROKER_CONNECTION_RETRY = True,
    # BROKER_CONNECTION_MAX_RETRIES = 100,
    # BROKER_LOGIN_METHOD = 'AMQPLAIN',
    # BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 18000},  # 5 hours

    # CELERY_ALWAYS_EAGER = False,
    # CELERY_EAGER_PROPAGATES_EXCEPTIONS = True,
    # CELERY_IGNORE_RESULT = False,
    # CELERY_MESSAGE_COMPRESSION = 'gzip' #bzip2,
    # CELERY_ACKS_LATE = True,
    # CELERYD_MAX_TASKS_PER_CHILD = 0,
    ADMINS = [('tycho','tychogrouwstra@gmail.com')],
    CELERY_SEND_TASK_ERROR_EMAILS = True,

    CELERY_RESULT_EXCHANGE = 'celeryresults',
    CELERY_RESULT_EXCHANGE_TYPE = 'direct',
    CELERY_DEFAULT_EXCHANGE_TYPE = 'direct',
    # CELERY_RESULT_EXCHANGE_TYPE = 'fanout',
    # CELERY_DEFAULT_EXCHANGE_TYPE = 'fanout',
    CELERY_DEFAULT_DELIVERY_MODE = 'persistent',
    CELERY_DEFAULT_ROUTING_KEY = 'celery',
    CELERY_RESULT_PERSISTENT = True,
    CELERY_TASK_RESULT_EXPIRES = 18000,  # 5 hours

    CELERY_RESULT_BACKEND = 'amqp',
    # CELERY_RESULT_BACKEND = 'amqp://guest:guest@localhost:5672//',
    # CELERY_RESULT_BACKEND = 'db+postgresql://scott:tiger@localhost/mydatabase',
    # CELERY_RESULT_BACKEND = 'redis://:password@localhost:6379/0',
    # CELERY_RESULT_BACKEND = 'db+sqlite:///results.sqlite',
    # CELERY_RESULT_BACKEND = 'mongodb://192.168.1.100:30000/',
    # CELERY_MONGODB_BACKEND_SETTINGS = {
    #     'database': 'mydb',
    #     'taskmeta_collection': 'my_taskmeta_collection',
    # },
    # CASSANDRA_SERVERS = ['localhost:9160'],
    # CASSANDRA_KEYSPACE = 'tasks_keyspace',
    # CASSANDRA_COLUMN_FAMILY = 'tasks',

    # echo enables verbose logging from SQLAlchemy.
    # CELERY_RESULT_ENGINE_OPTIONS = {'echo': True},
    # use custom table names for the database result backend.
    # CELERY_RESULT_DB_TABLENAMES = {
    #     'task': 'myapp_taskmeta',
    #     'group': 'myapp_groupmeta',
    # },

    CELERY_DISABLE_RATE_LIMITS = True,
    # CELERY_TASK_RESULT_EXPIRES = 30 * 60,
    # CELERY_RESULT_DB_SHORT_LIVED_SESSIONS = False,
    # CELERY_REDIS_MAX_CONNECTIONS = 9000,

    CELERY_CACHE_BACKEND_OPTIONS = {'binary': True, 'behaviors': {'tcp_nodelay': True}},

    # pickle is more binary-efficient than JSON though Python-only
    CELERY_TASK_SERIALIZER = 'json',
    # CELERY_TASK_SERIALIZER = 'pickle',
    CELERY_RESULT_SERIALIZER = 'json',
    # CELERY_RESULT_SERIALIZER = 'pickle',
    CELERY_EVENT_SERIALIZER = 'json',
    # CELERY_EVENT_SERIALIZER = 'pickle',
    CELERY_ACCEPT_CONTENT=['json', 'yaml', 'pickle'],

    CELERY_TIMEZONE = 'Europe/Oslo',
    CELERY_ENABLE_UTC = True,

    # important to customize queues by task to separate things
    CELERY_QUEUES = (
        # Queue('default', Exchange('default'), routing_key='default'),
        # Queue('for_task_A', Exchange('for_task_A'), routing_key='for_task_A'),
        # Queue('for_task_B', Exchange('for_task_B'), routing_key='for_task_B'),
        Queue('tasks.add', Exchange('tasks.add'), routing_key='tasks.add'),
        Queue('tasks.echo', Exchange('tasks.echo'), routing_key='tasks.echo'),
        Queue('tasks.urlopen', Exchange('tasks.urlopen'), routing_key='tasks.urlopen'),
    ),
    # CELERY_QUEUES = map(lambda k: Queue('tasks.' + k, Exchange('tasks.' + k), routing_key='tasks.' + k), TASKS),

    # separate, and preferably use priorities too!
    CELERY_ROUTES = {
        # 'my_taskA': {'queue': 'for_task_A', 'routing_key': 'for_task_A'},
        # 'my_taskB': {'queue': 'for_task_B', 'routing_key': 'for_task_B'},
        # 'tasks.urlopen': 'low-priority',
        # 'tasks.add': {'exchange': 'C.dq', 'routing_key': 'w1@example.com'},
        'tasks.add': {'queue': 'tasks.add', 'routing_key': 'tasks.add'},
        'tasks.echo': {'queue': 'tasks.echo', 'routing_key': 'tasks.echo'},
        'tasks.urlopen': {'queue': 'tasks.urlopen', 'routing_key': 'tasks.urlopen'},
    },
    # CELERY_ROUTES = dict(zip(TASKS,
    #     map(lambda k: {'queue': 'tasks.' + k, 'routing_key': 'tasks.' + k}, TASKS)
    # )),

    # celery worker -E -l INFO -n workerA -Q for_task_A
    # celery worker -E -l INFO -n workerB -Q for_task_B
    
    CELERY_REDIRECT_STDOUTS_LEVEL = 'INFO',

    # def my_on_failure(self, exc, task_id, args, kwargs, einfo):
    #     print('Oh no! Task failed: {0!r}'.format(exc))
    #
    # CELERY_ANNOTATIONS = {
    #     '*': {'on_failure': my_on_failure},
    #     # 'tasks.urlopen': {'rate_limit': '10/m'}
    # },

    # class MyAnnotate(object):
    #
    #     def annotate(self, task):
    #         if task.name.startswith('tasks.'):
    #             return {'rate_limit': '10/s'}
    #
    # CELERY_ANNOTATIONS = (MyAnnotate(), {...}),

    # CELERY_IMPORTS = ('tasks', ),

    # CELERYD_CONCURRENCY = number_of_cores,
    # CELERYD_PREFETCH_MULTIPLIER = 4,
)

if __name__ == '__main__':
    app.start()
