# -*- coding: utf-8 -*-
import re

def curl(url, headers={}):
    hList = headers.keys.map(lambda _:
        ' -H "%(_)s: %(headers[_])s' % locals()
    )
    hJoined = "".join(hList)
    return 'curl "%(url)s"%(hJoined)s -v' % locals()

def check_lengths(item, fields):
    size = len(item[fields[0]])
    for x in fields:
        assert len(item[x]) == size, 'Field [' + x + '] has length ' + str(len(item[x])) + ', field [' + fields[0] + '] has length ' + str(size) + '!'

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def rx_between(resp, behind, before):
    regex = '(?<=' + re.escape(behind) + ').*?(?=' + re.escape(before) + ')'
    return resp.re(regex)
