# Architecture

* Scraping: [Dispatch](http://dispatch.databinder.net/Dispatch.html)? ~~[Unfiltered](https://github.com/unfiltered/unfiltered) [Netty](https://github.com/netty/netty)? Python's `requests`?~~
* Message Queue: **Kafka** (+ Hermes?)
* Threading: streaming engine ~~[Quasar](https://github.com/puniverse/quasar)? Kilim? Celery with `gevent`?~~
* Extracting: **Parsley**
* Workflow engine: Spark ML Pipelines? Mario? Luigi? Airflow?
* Filter-queryable storage in front of queue: document DB? Spark RDDs? Spark-based throttling / rate limiting?
* Serialization: default Java serialization during testing; for production Kryo?; legible Thrift/JSON for debugging
* Batch RDD persistence storage formats: Avro during ETL, columnar DB or Parquet after?
* Data store:
	* relational: PostgreSQL?; PaaS: RDS
		* columnar: MemSQL?; PaaS: Redshift; + Kylin?
	* k/v: Redis? Riak? Pistachio?; PaaS: SQS
		* column k/v?: HBase? Cassandra?
	* files: HDFS?; PaaS: S3
	* document: RethinkDB? MongoDB? CouchDB? PostgreSQL?; XaaS: DynamoDB/SimpleDB
	* graph: Spark GraphX? Neo4j? Giraph? [Akka](http://letitcrash.com/post/30257014291/distributed-in-memory-graph-processing-with-akka)?
	* Merger DB: SploutSQL? Voldemort? HBase? Druid? ElephantDB? Riak? Pinot?
* Batch engine: Spark Dataframes? ~~Flink? + Tachyon (+ in-dev [Succinct](https://github.com/amplab/succinct))? Scalding (through Summingbird)? Scrunch?~~
* Stream engine: Spark Streaming? ~~Akka? Flink? Storm/Heron thru Summingbird? Samza?~~
* SQL engine: **Spark?** Presto? Hive? HAWQ? Impala? InfiniDB? Geode? Pinot?
* ~~Query abstraction: MRQL? Summingbird? Tez? Musketeer? Lens?~~
* Notebook: Hue 3.8? Spark Jobserver? iScala? iPython Notebook (Jupyter)? Zeppelin? **Spark Notebook**?
* Service containers: Docker + Kubernetes ~~Docker Swarm?~~
* OS: [Mesosphere DCOS on CoreOS](https://docs.mesosphere.com/tutorials/mesosphere-on-a-single-coreos-instance/)
* Deployment / Provisioning: Ambari? Cloudera Manager? CloudBreak? ~~Chef?~~ AWS CloudFormation? BigTop?
* Cluster management: Weave? Fabric? Ganglia? Zookeeper?
* Testing: ScalaCheck, Luigi Python unit tests, Jenkins, sbt ~, errors as values, SPoFs, stateful parts, Simian Army.
* REST API: Netflix Genie
* Search: ElasticSearch; PaaS: CloudSearch
* ML: PMML? Spark MLLib/KeyStoneML? see Solutions!ML/Lang-Topic; PaaS: AWS ML, Azure ML. Deploy with [Velox](https://github.com/amplab/velox-modelserver).
* NLP: ML like Spark MLLib's Word2Vec? see ditto.
* Cloud abstraction: LibCloud?
* DAX: DirectQuery

~~If I fail to obtain added value from RethinkDB + RabbitMQ, fall back to PostgreSQL (RDS) + Redis (SQS) for the superior AWS integration! (I think RethinkDB + Redis wouldn't work, nor PostgreSQL + either RabbitMQ/Redis...)~~

# Issues

#### Kafka index:
* if no backlog/index, no reprocessing possible
* if queue index stored:
	* in consumers once per consumer group, then a resetting node would have forgotten where they left off.
	* in consumers once in each worker node, syncing issues?
	* ~~in Kafka once, then it wouldn't work for distinct consumer groups~~
	* in Kafka once per consumer group, then do old counters for previously completed tasks ever get cleared out?

#### Rate limiting:
- :warning: generalize one of these to keep ticks left by domain key: use the key-based accumulators?
- Akka:
	- [tick-based FSM (!); TimerBasedThrottler](http://letitcrash.com/post/28901663062/throttling-messages-in-akka-2)
	- [zip with tick source](https://github.com/pkinsky/akka-streams-example/blob/master/src/main/scala/WordCount.scala)
- Spark:
	- Akka solutions through [ActorHelper](http://spark.apache.org/docs/latest/streaming-custom-receivers.html)
	- [BlockGenerator](https://github.com/apache/spark/pull/945/files) - spark.streaming.receiver.maxRate, use one receiver per domain to consume from kafka?
		- no customization by domain
		- is separate receivers default or harder?
	- on Akka's tick-based FSM
		- FSMs are implemented in Spark as accumulators (but write-only!), so no use
		- sleeping when caught up: `Thread.sleep(ms);`
		- ticks as DStream batch size duration?
		- :question: no way to join the tick stream except Akka style?
			- but no key-based accumulators or DStreams at that level for distinguishing domains?

#### Architecture for multi-IP (and multiple nodes per IP) cluster

- ~~'1 node per IP' assumption would allow local tallies; fetch new URLs from domain-based central queues as required~~
- otherwise requires nodes to know their IP for central tally (key-based Spark accumulators)
	- API tally: api, key
	- web tally: domain, IP
	- would local IP suffice to distinguish?
		- yet remain fixed relative to the external IP?
			- fair assumption to start with
- how will an IP-sensitive tally relate to queues?
	- throttling central queue makes no sense, as some IPs may still be able to take more of some domain
	- centralized responsibility to distribute
		- separate queues by IP
			- fairly distribute work by domain to the IPs?
				- related: RabbitMQ's 'fair dispatch' on consumers: `channel.basic_qos(prefetch_count=n)`
				- wait, distribution does not suffice as politeness throttling
		- centralized responsibility to throttly-distribute
			- like current RethinkDB approach but per IP
			- preferably fairly distributing due work
			- possibly also taking into account existing IP-based queue sizes
				- as percent of expected capacity?
		- distribute centrally, throttle/tally locally per IP (each node a tally DB + script?)
	- localized responsibility to distribute
		- separate queues by domain?
	- semi-localized responsibility to distribute: one script per IP
	- for Spark:
		- could use Spark accumulators with key {domain, local IP / API key} to do cluster-wide tally of what's due where
		- could use domain-segregated Kafka queues to give control over what to take when...
		- could use above throttling/rate-limiting means to control working speed per combination...
		- how to control distribution of work over the cluster then?
			- Spark Scheduler?... wait, no, not easily possible?
			- run separate Spark streaming apps over nodes in the cluster connected through common Kafka
				- no cluster-wide accumulators, but given "1 Spark app per IP" assumption this means native segregation by IP, making this easily suited for web scraping (as opposed to API scraping which requires global accumulators across the job cluster) using domain-segregated Kafka queues and throttlers.

#### Politeness:
- RethinkDB being a glorified queue not doing its real-time query job
- RethinkDB regular query returning no results either:
	- just ditch RethinkDB/RabbitMQ and swap them out for PostgreSQL/RDS (in a loop of manual querying) and Redis/SQS?
	- wait, first rethink architecture for multi-IP (and multiple nodes per IP) cluster
- ~~error emails: set SMTP -> http://docs.celeryproject.org/en/latest/configuration.html#error-e-mails~~

	#### Batch workflow engines like Luigi:
	* added value over just Kafka?
		* dependent task invalidation based on time/whatever?
		* as glue for like viz?
	* automate?

	#### Error handling:
	- errors as values approach: great way to quarantine error damage, see what's good and what isn't when things go bad
	- exception approach: easy to trace errors
	- Haskell: [Either, fail, MonadError + throwError, throwDyn](http://www.randomhacks.net/2007/03/10/haskell-8-ways-to-report-errors/) [(now just called throw/catch); ](http://blog.ezyang.com/2011/08/8-ways-to-report-errors-in-haskell-revisited/)
	- Scala:
		- unhandled: in.map(_.toInt)
		- map/filter + try: `val out = in.map(a => Try(a.toInt)); val results = out.filter(_.isSuccess).map(_.get)`
		- flatMap: `rdd.flatMap(r=> {if (NumberUtils.isNumber(r)) Some(r.toInt) else None})`
		- map/filter + try/catch: `rdd.map(r => { try{ r.toInt } catch { case rte: RuntimeException => { -1 } } }).filter(a => A != -1)`
		- flatMap + try/catch: `rdd.flatMap(r=> {try{ Some(r.toInt) } catch { case rte: RuntimeException => { None } } })`
		- Option (Haskell's Maybe): `val opt:Option[A] = ...; val result:B = opt.map(f).getOrElse(g());`
		- Option.fold: `opt.fold(def)(f)`
		- custom maybe: `
		implicit def optionWithMaybe[A](opt: Option[A]) = new {
			def maybe[B](g: =>B)(f: A=>B) = opt map f getOrElse g
		}
		opt.maybe("") { _.toUpperCase }
		`
		- Twitter's [**Try/Success/Failure**](http://www.scala-lang.org/files/archive/nightly/docs/library/index.html#scala.util.Try)
	- Akka: hierarchical escalation
	- Spark:
		- Exceptions should be sent back to the driver program and logged there (with a SparkException thrown if a task fails more than 4 times). The log output will go into the stdout and stderr files in the work directory on your worker. You can also access those from the Spark cluster’s web UI (click on a worker there, then click on stdout / stderr).
	- Storm: ?
	- Summingbird: ?
	- Flink: ?


# Feature List

- Scraper considerations: [`requests`](http://docs.python-requests.org/en/latest/) as an alternative to scrapy
	- ~~scraping -- both~~
	- ~~concurrency -- Celery~~
	- ~~start_urls: either~~
		* ~~curl -X POST -d '{"args":["http://www.reddit.com/r/asoiaf/"]}' http://localhost:5555/api/task/send-task/tasks.urlopen~~
		* ~~add to the right RabbitMQ queue for Celery task, i.e. tasks.urlopen -> tasks.urlopen~~
	- politeness delays
		- APIs: enforce by global tally (RethinkDB + `get.sql`)
		- web:
			- ~~limit threads -- easiest but lowest potential~~
			- screw IPs -- do central domain tally **<- current**
			- have threads know their IP, do central IP-domain tally
			- proxies/Tor -- most work but potential to break limits
			- ~~spawn_after(seconds)? -- wait, the next url doesn't wait for this thread anyway~~
	- callbacks:
		- Greenlet.link_value(callback)
		- message queue pub/sub
	- extraction/items -- parsley
	- next url push -- push onto queue in parsley callback?
	- ~~redirect following -- both~~
		- redirect customization -- [`r.history`](http://docs.python-requests.org/en/latest/user/quickstart/#redirection-and-history)
	- dupe filter -- central bloom filter
		- bypassing dupe filter -- if bypass:true, don't check bloom filter

# Feature Options

- [Queues](http://queues.io/) -- **Kafka, maybe with document DB in front to filter input**
	- see [SO](http://stackoverflow.com/questions/731233/activemq-or-rabbitmq-or-zeromq-or), [Second Life evaluation](http://wiki.secondlife.com/wiki/Message_Queue_Evaluation_Notes) from 2009:
	- [Kafka](https://kafka.apache.org/): Scala, out-performs 1-node SQS, more so with batched messages; no Celery.
		* dedupe for [exactly-once guarantee](https://cwiki.apache.org/confluence/display/KAFKA/FAQ#FAQ-HowdoIgetexactly-oncemessagingfromKafka) .. or just use Spark's Kafka direct API
		* topics for data type
		* partitions for grouping/filtering; [re-partition](http://stackoverflow.com/a/28702025/1502035) by different dimension if needed.
			* :warning: So no filtering subscription by 2+ dimensions from Kafka directly?
			* how do you re-partition? can this even be done for what's already in there, or should info be reloaded into Kafka?
		* [Hermes](https://github.com/allegro/hermes): asynchronous REST message broker built on top
			* Angular GUI [Hermes Console](http://hermes-pubsub.readthedocs.org/en/latest/contents/user/console.html) -- not open-sourced yet :(
		* Best [durability](http://www.slideshare.net/ptgoetz/apache-storm-vs-spark-streaming) guarantees against data loss.
		* [camus](https://github.com/linkedin/camus) for Kafka to HDFS?
		* [kafka-manager](https://github.com/yahoo/kafka-manager)
		* [scala-kafka](https://github.com/stealthly/scala-kafka)
		* Kafka consumers just maintains an offset, so reprocessing just means restarting at offset 0
			* maybe keep old results until new finishes
			* Samza simple/parallel [rewind](http://samza.apache.org/learn/documentation/0.7.0/jobs/reprocessing.html)
				* Kafka history: **db changes** (key) vs. activity events
					* [log compaction](http://kafka.apache.org/documentation.html#compaction): ditch old values for key
			* [![](http://s.radar.oreilly.com/wp-files/2/2014/06/kappa.png)](http://radar.oreilly.com/2014/07/questioning-the-lambda-architecture.html)
		* how to use with:
			* [Samza](https://github.com/apache/samza-hello-samza/tree/master/src/main/java/samza/examples/wikipedia)/[Storm](https://github.com/apache/storm/blob/master/examples/storm-starter/src/clj/storm/starter/clj/word_count.clj): built for it
			* Akka: [Akka-Kafka](https://github.com/sclasen/akka-kafka), [reactive-kafka](https://github.com/softwaremill/reactive-kafka)
			* Spark: [0](https://spark.apache.org/docs/1.2.0/streaming-kafka-integration.html), [1](https://databricks.com/blog/2015/03/30/improvements-to-kafka-integration-of-spark-streaming.html), [2](https://github.com/apache/spark/blob/master/examples/scala-2.10/src/main/scala/org/apache/spark/examples/streaming/KafkaWordCount.scala), [3](https://github.com/apache/spark/blob/master/examples/scala-2.10/src/main/scala/org/apache/spark/examples/streaming/DirectKafkaWordCount.scala)
			[4](https://github.com/dibbhatt/kafka-spark-consumer)
			* Spark's Kafka [direct](https://spark.apache.org/docs/1.3.0/streaming-kafka-integration.html) [API](https://databricks.com/blog/2015/03/30/improvements-to-kafka-integration-of-spark-streaming.html)
			* [Summingbird](https://github.com/kscaldef/summingbird-hybrid-example/blob/master/src/main/scala/com/twitter/tormenta/spout/KafkaSpout.scala)
			* Scala: [Octobot](https://github.com/cscotta/Octobot)
			* [Flink](http://ci.apache.org/projects/flink/flink-docs-master/apis/streaming_guide.html#apache-kafka)

	- [0MQ](http://zeromq.org/): no persistence, DIY, much faster with many small messages; no Celery.
		- AMQP offshoot that learned from their mistakes
		- has 'pipeline' primitive
	- nanomsg: 0MQ offshoot
	- [Amazon SQS](https://aws.amazon.com/sqs/): out-performs RabbitMQ, Celery bindings immature.
	- [RabbitMQ](https://www.rabbitmq.com/): Erlang, AMQP, most popular?, great but slower than 0MQ, standard for Celery.
		- :-1: issue: can't copy queues or read them without consuming! :(
			* exchange to specific thing -> fanout exchange -> archive (always read with no-ack) to copy to new queue (do ack) on the same exchange
				* only works as long as the queue is shorter than the ack cooldown
	- ~~Apache ActiveMQ: Java, middle ground~~
	- ~~Beanstalkd: written for FB in C; no persistent queues?~~
	- [Redis](http://redis.io/) / [Amazon ElastiCache](https://aws.amazon.com/elasticache/)
		- :o: [scrapy-redis](https://github.com/darkrho/scrapy-redis): distributes Scrapy URLs from Redis queues (one per spider name) to worker nodes for scraping/processing
			* :warning: custom request headers and domain limits must be made part of sub-classed spiders
			* :question: if I want to use one versatile spider process per node, while each spider has its own queue, should I consolidate spider classes into one?
				- if not, consider `scrapyd` for adding spiders?
				- :warning: suddenly requires to emulate distinct spiders and pass on this info with the URLs...
				- :question: does this optimization justify performance gained?
					- :-1: wouldn't distinct spiders help to ensure uncommon domains don't get stuck at the back of the Redis queue while polite to scrape already behind URLs of a common domain that could use some cooldown?
						- considering delay option per domain, it shouldn't sleep the entire queue, but instead just throws over-scraped domains back to the end of the queue. this rearrangement may still cost a bit of time though...
			- [example](http://www.seckintozlu.com/1148-how-to-run-scrapy-spiders-on-the-cloud-using-heroku-and-redis.html) with Scrapyd and Heroku
		- ~~[Redis Queue](http://python-rq.org/) (RQ)~~
			- ~~light Celery alternative, not distributed, no webhooks~~
		- evenly distributing over Redis cluster: hash(key) % nodes; hash(key) % threads
			- thread number coprime to node number (or different hash function)... always good if prime, unless the same number
	- :star: [Celery](https://github.com/celery/celery): Python's Distributed Task Queue (more like glue between queue and threading)
		- use canvas primitives (see celery/readme.md) like [chain](https://gist.github.com/codeinthehole/4124910)
		- [celery-pipelines](https://github.com/bolster/celery-pipelines)?
		- no Scrapy example, just `requests` version
		- :question: what of scrapy's native per-domain politeness delays and concurrency?
			- could just edit in in scrapy-redis... worth it?
			- actually, politeness for APIs could be enforced by global tally anyway; for web, limit threads until proxies?
			- checked list of scrapy features now, every feature can be done with `requests`
		- :star: use `RabbitMQ` (maaaybe `SQS`) as message transport
		- use one `Celery` worker per CPU, 1k eventlets/greenthreads each
		- examples:
		 	- [Celery scrape](https://github.com/celery/celery/blob/master/examples/eventlet/webcrawler.py)
				- D:\shared\celery\examples\eventlet
			- [legco-watch](https://github.com/legco-watch/legco-watch)
				- scrape job properties: spider name, scheduled time, job id, raw response, completed time (set on callback), last fetched time
		- You shouldn't pass Database objects (for instance your User model) to a background task because the serialized object might contain stale data. What you want to do is feed the task the User id and have the task ask the database for a fresh User object.
		- [Jobtastic](http://policystat.github.io/jobtastic/): progress bars
	- Disque: in-memory, distributed Redis-like job queue (alpha)
	- :star: custom queue DB like RethinkDB: `dbs.sql`, `get.sql`
		- :o: allows specifying api key (for api scrape), domain/spider (for headers for web scrape; next polite time / hourly limit/tally), helps query queue for polite domains only, can mimic logic from scrapy-redis.
		- :question: tallies complicated by api key / IP separation?
		* if queues like RabbitMQ cannot both stream and retain work, would `gevent` + `RethinkDB` be a better fit?
			* :-1: not even really meant for this...


- queue/streaming requirements:
	- queryable json
	- distribute work
	- subscribe from start


- Stream processing: -- **RethinkDB; potentially with Spark (StreamSQL) as backup**
	- :question: [Storm](https://storm.apache.org/documentation/Tutorial.html):
		- guaranteed message delivery!
		- tuple-based API (probably PQ par)
		- Java, verbose?
			- slightly improved upon by the [Trident](https://storm.apache.org/documentation/Trident-tutorial.html) streams API enabling exactly-once semantics by essentially doing a de-dupe
			- better thru the collection-based Summingbird's Scala API?
			- successor [Heron](https://blog.twitter.com/2015/flying-faster-with-twitter-heron) backward compatible with Storm API, so should work with Summingbird too!
		- [scraping example](https://github.com/tjake/stormscraper/blob/master/src/main/java/com/github/tjake/stormscraper/)
	- :o: [Spark Streaming](https://spark.apache.org/streaming/): [decent API](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.streaming.api.java.JavaDStream).
		- could even stream [DataFrame operations](https://news.ycombinator.com/item?id=9064486)!
		- data sources considered reliable: S3, HDFS, [pull-based Flume](https://spark.apache.org/docs/latest/streaming-flume-integration.html), Kafka acked at the right time (after processing and receiving ack from next Kafka sink)?
	- :x: [SQLStream](http://www.sqlstream.com/stream-processing-with-sql/): SQL, but seems closed/commercial.
	- :o: [StreamSQL](https://github.com/thunderain-project/StreamSQL): SQL on streams with Spark
	- :star: RethinkDB's [ReQL](http://rethinkdb.com/docs/sql-to-reql/) queries
		- map to SQL as was [previously](http://querymongo.com/) done for [MongoDB](https://github.com/master/mongosql)?
		- [changefeeds](http://rethinkdb.com/docs/changefeeds/python/): subscribe to change feeds; less scalable than below queues if serving many clients.
		- [repubsub](http://rethinkdb.com/docs/publish-subscribe/python/): exchange with queues on hierarchical topics, allowing customization of who wishes to receive what
		- [RabbitMQ](http://rethinkdb.com/docs/rabbitmq/python/) integration
		- probably works with any other equally well -- for Celery I was submitting jobs through Flower's HTTP interface anyway...
	- PaaS: [Amazon Kinesis](http://aws.amazon.com/kinesis/)
	- [Akka](https://github.com/pkinsky/akka-streams-example) - core of Spark, handles actors and data flows in distributed clusters with persistance
		- [scraping example](https://devcenter.heroku.com/articles/scaling-out-with-scala-and-akka), [spider](https://github.com/typesafehub/webwords/blob/master/indexer/src/main/scala/com/typesafe/webwords/indexer/SpiderActor.scala)
		- back-pressured asynchronous stream processing (= pub/sub sides communicate demand/capacity)
		- can talk to other implementations (Pivotal Reactor, Reactive Rabbit, rxJava, vert.x) over the Reactive Streams protocol.
		- receive requests rather than taking work for a queue like Celery threads
		- integrate with queues through [mailbox semantics](http://doc.akka.io/docs/akka/snapshot/scala/mailboxes.html)?
			- different approach with [RabbitMQ](http://www.typesafe.com/activator/template/rabbitmq-akka-stream)
		- lacks Storm's [guaranteed message delivery](http://architects.dzone.com/articles/akka-vs-storm)
			- is this even of interest if Kafka already offers it?
		- common patterns:
			- Rate limiting:
				- [tick-based FSM (!); TimerBasedThrottler](http://letitcrash.com/post/28901663062/throttling-messages-in-akka-2)
				- [zip with tick source](https://github.com/pkinsky/akka-streams-example/blob/master/src/main/scala/WordCount.scala)
				- the tick-based FSM seems interesting in that it can accumulate 'vouchers' for scrapes left; could limit this to say max 10 to prevent too many simultaneous scrapes too if behind for a bit. is this state shared across actor instances though? how could I ensure one state per IP... especially if I cannot guarantee number of VMs per IP?
			- [Balancing Workload Across Nodes](http://letitcrash.com/post/29044669086/balancing-workload-across-nodes-with-akka-2): BalancingDispatcher with the stipulation that the Actors doing the work have distinct Mailboxes on remote nodes
			- [Akka Work Pulling Pattern](http://www.michaelpollmeier.com/akka-work-pulling-pattern/) to prevent mailbox overflow, throttle and distribute work. It also let’s you distribute work around your cluster, scale dynamically scale and is completely non-blocking. Specialisation of the above ‘Balancing Workload Pattern’.
			- [Shutdown Patterns](http://letitcrash.com/post/30165507578/shutdown-patterns-in-akka-2): tell Akka to shut down the ActorSystem when everything’s finished (tl;dr: distribute PoisonPills, wrapped for routers)
			- [caching / memoization](http://letitcrash.com/post/30509298968/case-study-an-auto-updating-cache-using-actors)
			- [use with AMQP](http://letitcrash.com/post/29988753572/akka-amqp-proxies)
	- Flink: tupled-based (datasets), both batch/streaming
		- use [DataSet](http://ci.apache.org/projects/flink/flink-docs-release-0.8/api/scala/index.html#org.apache.flink.api.scala.DataSet)s based on [case classes](http://ci.apache.org/projects/flink/flink-docs-master/apis/programming_guide.html)
		- lambda-based [transformations](http://ci.apache.org/projects/flink/flink-docs-release-0.8/dataset_transformations.html): map, flatmap, mapPartition, filter, reduce, reduceGroup, aggregate, join, coGroup, cross, union, partitionByHash, sortPartition, first(n); reduce, select; groupBy, where
	- [Project Apex](https://www.datatorrent.com/product/project-apex/): bold claims but no source links yet, much less Summingbird bindings...


- ~~Scrapyd: allows uploading spider requests to your cluster and schedules/deploys them, so for triggering spiders whereas queues like Celery are for feeding URLs~~
	- ~~curl http://localhost:6800/schedule.json -d project=myproject -d spider=spider2~~
	- ~~max_proc and max_proc_per_cpu options~~

- [Overwritable functions](https://gist.github.com/saidimu/1024207):
	- start_requests(): populate (initial) URLs from DB
		- not needed with `scrapy-redis`?
	- ~~parse(): callback function; return any Item and/or Request objects~~
		- ~~to generate 'next' URLs maybe use [link extractors](http://doc.scrapy.org/topics/link-extractors.html)~~
			- relative urls:
			```python
			base_url = get_base_url(response)
			abs_url = urljoin_rfc(base_url, relative_url)
			```

* Serialization: default Java serialization during testing; for production Kryo?; legible Thrift/JSON for debugging
	* Thrift: legible serialization + RPC stack
	* ProtoBuff (Protocol Buffers): language neutral serialization format popularized by Google, like bson with schema + enumerations
	* Cap'n Proto (protobuff improvement)
	* Msgpack: middle of the pack
	* Kryo: improves on default Java serialization for Spark, but must register classes
		- spark.serializer = org.apache.spark.serializer.KryoSerializer; spark.kryo.registrationRequired = true
		- generate class registering code to automate?
	* ![](http://drill.apache.org/images/home-json.png)

* Batch RDD persistence storage formats: Avro during ETL, columnar DB or Parquet after?
	* Avro: serialization? + row-oriented storage format (this part can be swapped out with columnar Parquet); modeled around ProtoBuff but for the Hadoop ecosystem; recommended by Kafka expert Confluent
		- Kafka Avro [schema registry](https://github.com/confluentinc/schema-registry) by [Confluent](http://confluent.io/docs/current/quickstart.html)
	* Parquet - columnar storage format, not actually serialization.

* SQL engines:
	* Open:
		* Apache Hive: HiveQL to MR, getting Tez (formerly Stinger) support for speed from Hortonworks
		* Cloudera Impala: interactive SQL/HiveQL over HDFS/HBase, must store as Parquet
		* Facebook Presto: interactive SQL joining across Hive/HBase/Cassandra/DBs, successor of FB's Hive, must store as RCFile
		* Shark -> Spark SQL: most of HiveQL
		* EMC/Pivotal HAWQ: fast ANSI SQL
		* InfiniDB: MySQL-ish columnar DB for interactive SQL on Hadoop
		* Apache Drill: query NoSQL stores, pushed by MapR
		* Apache Sqoop: import from DBs, integrates with HCatalog for Pig/Hive
		* Apache Phoenix: interactive SQL (r/w) over HBase
		* Cascading Lingual: ANSI SQL
		* CitusDB: SQL on Hadoop interactive querying with PostgreSQL
		* Apache Tajo: ANSI SQL
		* Apache Geode: open-source version of Gemfire
		* [LinkedIn Pinot](https://github.com/linkedin/pinot): columnar DB for single-table SQL queries
	* Commercial:
		* Hadapt: interactive SQL querying on Hadoop
		* Jethro: fast non-intrusive SQL-on-Hadoop via auto-indexing
		* Oracle Big Data SQL: Hadoop + NoSQL like Drill
		* BigSQL by IBM: PostgreSQL + Hadoop
		* Pivotal Gemfire

	Query abstraction:
	* Cascading/Scalding(!)/Cascalog
		* [Summingbird](https://github.com/twitter/summingbird): run Scala/Java collection transformations on Scalding (batch), Storm/Heron (stream), and Spark (experimental)
	* Spark - operations from one RDD to another?
		* try col DB like [MemSQL Community](http://www.memsql.com/download/) as backend thru [connector](https://github.com/memsql/memsql-spark-connector)
	* [Crunch](http://crunch.apache.org/)
		* based on Google's closed dataframe-like [FlumeJava](https://stephenholiday.com/notes/flumejava/)
		* runs on Spark and MR
		* Scala API through [Scrunch](http://crunch.apache.org/scrunch.html)

- storage:
	- backends: -- **RethinkDB**
		- Databases
			- :star: document stores, e.g. RethinkDB
				- supports query streaming
			- [RDS](http://aws.amazon.com/rds/) / RDBMSs through [SQLAlchemy](http://sqlalchemy.org/) ([1](http://stackoverflow.com/questions/23360801/loading-heroku-pg-database-with-scraped-data-from-scrapy-spider), [2](https://github.com/ajkumar25/HNScrapy/tree/master/hn))
				- :o: good findability (filter any columns)
				- :question: ETL callbacks unknown
				- supports Celery... for Django ORM?
				- RDS (PostgreSQL) pricing: Virginia/Oregon db.t2.micro @ $0.018 / hour; MySQL $0.017; $0.10 / 1M IOs
				- Aurora bit pricier and min. db.r3.large but supposedly 5x outperforms MySQL; IOs $0.20 per million requests.
				- storage $0.10 / GB-month.
				- ~~Portia: storing in [MySQL db](https://github.com/scrapinghub/portia#mysql-backend-for-projects)~~
		- Redis (persist through [RDB](http://redis.io/topics/persistence)) / Amazon [ElastiCache](https://aws.amazon.com/elasticache/)
			- :o: queue for callbacks
			- works with Celery thru AMQP
			- ElastiCache pricing: Virginia/Oregon cache.t2.micro @ $0.017 / hour
				- :x: storage not mentioned?? don't use to store response bodies then?
			- the Redis FAQ [recommends](http://redis.io/topics/faq) combining Redis with a DB backend for persisting bigger blobs
			- using keys e.g. 'spider:category:date:pagenum', or `rpush` as list/set elements (without pagenum)
				- then `PSUBSCRIBE` by pattern for streaming; `full iteration` of `MATCH` pattern in [`SCAN`](http://redis.io/commands/scan) for batch, like RabbitMQ's [route_key wildcards](https://www.rabbitmq.com/tutorials/tutorial-five-python.html).
		- S3: changes may not show immediately, higher latency but can be accessed from anywhere simultaneously
			- :question: unknown callbacks (Oozie could work but no sign of people who have)
			- bit pricier?
			- :question: unknown 'eventually consistent' implications
				- not aware of immediate problems if not used for like current page count, current request tally, time to next polite fetch.
			- $0.14 / GB-month *used*, $0.01 / 10k GETs, $0.01 / 1k PUTs
		- ~~EBS: can be mounted as device by EC2 (one at a time), no resizing, cheaper I/O but charges even unused space, more fallible than S3, single availability zone~~
			- :-1: at best useful as buffer to batch io to S3 for io costs
			- $0.10 / GB-month *provisioned* and $0.01 / 100k IOs
		- ~~PersistentFS: like mounting S3 as EBS.~~
		- ~~Cloudfront: fast (replicated across globe), so best fit for small files that need to load fast (i.e. CDN for static assets)~~
		- ~~HDFS + DBs: redundant, non-persistent~~
		- pricing
			- :o: json logs? on EBS io-frugal
			- S3 bit worse?
		- ACID persistence
			- :x: EBS (json logs?) ephemeral
			- :x: no extra storage on ElastiCache?
			- others OK
		- findability
			- :o: DB great
			- S3/Redis hierarchical
			- :x: json log and default scrapy-redis queue-only?
		- callbacks
			- :question: S3?
			- :o: Redis as next queue
			- :question: PG unknown
			- fallback scrapy callbacks?
		- speed
			- :x: S3 'eventual'?

	- storage methods: -- **non-scrapy callback**
		- store in DB by key in:
			- non-scrapy callback
			- `scrapy_redis.pipelines.RedisPipeline` -- scrapy-redis default
				- :question: what of findability if all under one key?
			- pipeline `process_item()` -- scrapy default
				- good if you want to use scrapy items
					- seems terrible for storing response body
					- depends on scrapy
			- `parse()` callback -- response url/body available even if not part of item
		- json logs:
			- emit items + scrapy crawl tb -o items.json
				* array-like so seems suitable to hold results for paginated lists...
				* :question: is JSON right for storing full page content?
				* :x: even if stored externally, will this retain page progress (ACID compliance) if a node dies half-way through? How to resume?
				* :x: no page/url key, so susceptible to duplicates?
- politeness delays: -- **API: RethinkDB + `get.sql`; web: **
	- API scraping: separate delay by OAuth keys
		- tally approach: `get.sql` has logic for this
			- :o: complements Celery
			- ~~competes with Scrapy's built-in tracking (used in scrapy-redis)~~
	- web scrape:
		- `scrapy`'s [spider-based delay customization](https://scrapyd.readthedocs.org/en/latest/api.html)
			- `scrapy_redis.scheduler.Scheduler`
			- ~~[`celery.schedules`](http://docs.celeryproject.org/en/latest/reference/celery.schedules.html)~~
		- should track a separate limit for each IP (unlike IP-agnostic API scrape). Implies separate local DB per IP (is that how it works in scrapy-redis? how could this be done with Celery?), but if there are multiple VMs on the same IP (Docker approach), the '1 node = 1 IP' assumption no longer holds...
			- bypassed using IP rotation for web scrape (ditch delays); API scrapes would still require a central queue
				- IP rotation:
					- [proxy rotation](http://stackoverflow.com/questions/20792152/setting-scrapy-proxy-middleware-to-rotate-on-each-request)
					- [Tor](https://pkmishra.github.io/blog/2013/03/18/how-to-run-scrapy-with-TOR-and-multiple-browser-agents-part-1-mac/)
					- [Polipo](http://www.pps.univ-paris-diderot.fr/~jch/software/polipo/): caching/forwarding web proxy server
						- [Cloak](https://github.com/thnkr/cloak): use Heroku dynos as proxies with refreshable IP
	- Custom redirect handling
		- scrapy: overwrite [BaseRedirectMiddleware](https://github.com/scrapy/scrapy/blob/master/scrapy/contrib/downloadermiddleware/redirect.py)
		- requests: [`r.history`](http://docs.python-requests.org/en/latest/user/quickstart/#redirection-and-history)
- processing:
	- :question: How to trigger extractions/transformations when some info gets available? -- **message queue**
		- Oozie/Luigi workflow's ability to see when items are done
		- script like process_items.py grabbing finished items from a (central/local) *message queue* (like scrapy-redis's dmoz:items) for further processing
			- :question: would a message queue be less desirable than a DB in terms of persistence?
				- :o: nope, Redis already seems to persist
				- :o: RabbitMQ should be able to do this as well
				- otherwise use DB (Rethink?) as backend and queue over this
			- :question: I/O cost implications?
		- Scrapy's built_in process_item() callback
		- :question: what of for data stored in DBs? or used in conjunction with other callback triggers?
			- RethinkDB offers change streaming, repubsub and RabbitMQ integration
	- :question: How to trigger the appropriate callbacks even if items are entered from a generic queue? -- **meta-data + topic/query queues**
		- spider-bound: parse callback, pipeline process_item()
		- generic spider with dynamic parse callback distinguishing based on 'spider' type
			- distinguish using an item value?
			- just store meta data on data type (e.g. Taobao SKU list page) whether or not part of Scrapy
	- Extraction: -- **Parsley**
		- [Parsley](https://github.com/fizx/parsley): json-style XPath, CSS, regex in C
		- scrapy: ditto
- concurrency -- Celery's gevent
 	- scrapy's [CONCURRENT_REQUESTS_PER_DOMAIN](http://doc.scrapy.org/en/latest/topics/settings.html#std:setting-CONCURRENT_REQUESTS_PER_DOMAIN): affects delay, but not sure on specifics
	- Celery:
		- [gevent](http://www.gevent.org/): uses greenlets, inspired by eventlet but features more consistent API, simpler implementation and better performance
			- Start Celery worker with -P gevent
		- ~~[Eventlet](http://eventlet.net/)~~
		- ~~Prefork~~
- deduplication: :question: how to prevent duplicate url scrapes? -- :o: **consider ok for now**
	- `scrapy-redis`'s Redis `:dupefilter` does this
	- Celery: [BloomFilter](https://axiak.github.io/pybloomfiltermmap/) should be centralized if spidering for possibly duplicate links
	- :question: what if you wish to overwrite (certain) results after adjusting an extraction method?
	- :question: what if a URL is considered to have fleeting content and should be scraped each time a user wants it checked?
		- :o: [`Request(url, dont_filter:True)`](http://doc.scrapy.org/en/latest/topics/request-response.html) ensures a non-unique URL will still be accepted in the queue.
			- scrapy-redis respects this
	- :question: what if you wish to scrape a URL weekly?
		- :o: could add `&date=[date]` for each URL to make them unique...
	- :question: what if a URL should be called with different API keys?
		- :o: key probably part of URL

- ACID-proofing / verifying work:
	- RabbitMQ's ack(nowledgements) with Celery
	- also: [confirms](https://www.rabbitmq.com/confirms.html) (publisher acknowledgements)
	- ~~coordinator also puts work stamped with the time it was sent to a worker into a waiting queue invisible to workers~~
	- ~~coordinator sweeps unverified work from waiting queue and pushes them back to top of work queue~~
	- ~~pubsub protocols:~~
		- ~~[pubsubhubbub](https://code.google.com/p/pubsubhubbub/)~~

* Workflow engines: -- **Spark ML Pipelines? Mario?** Luigi? Airflow?
	* Comparisons [1](http://www.slideshare.net/jcrobak/data-engineermeetup-201309) (32-34), [2](http://bugra.github.io/work/notes/2014-04-13/workflow-engine-comparison-first-impressions/) -- Luigi wins
	* :+1: Luigi: job / data pipeline manager (in Python); jobs can be Hive, Pig, Hadoop, Spark, Cascading/Scalding, ...
		* wait, this isn't used for streaming so is probably useless to me, right?
		* how to not consume input?
			* wait, does this consume input files?
		* rerun [1](https://github.com/spotify/luigi/issues/595), [2](http://stackoverflow.com/questions/28793832/can-luigi-rerun-tasks-when-the-task-dependencies-become-out-of-date)
	* [Mario](https://github.com/intentmedia/mario): pipelines like Luigi but for Spark in Scala, with strong typing guarantees and complex dependencies unlike the linear Spark ML Pipelines
	* Azkaban: job manager, Hadoop only
	* Oozie (through YARN?): job manager, Hadoop only, yucky XML
		* [Falcon](http://hortonworks.com/hadoop-tutorial/defining-processing-data-end-end-data-pipeline-apache-falcon/) to trigger stuff in pipeline?
	* Chronos (through Mesos?)
		* can this do streaming with dependencies?
	* Pinterest Pinball
	* manually visualize pipeline:
		* Neoclipse -> neo4j GUI allowing icons for nodes
	* AirBnB Airflow

[Lambda architecture](http://lambda-architecture.net/components/)	of Storm/Spark for streaming + Hadoop/Cassandra/DB for batch?
	* [Oryx](http://oryxproject.github.io/oryx/): lambda architecture on Spark + Kafka specialized for real-time ML with PMML serialization
	* [Spark example](https://speakerdeck.com/mhausenblas/lambda-architecture-with-apache-spark)
	* [summingbird](https://github.com/twitter/summingbird) mentions a "[hybrid batch/realtime mode](https://github.com/kscaldef/summingbird-hybrid-example) that offers your application very attractive fault-tolerance properties".
	* [Merge DB](http://lambda-architecture.net/components/2014-06-30-serving-components/) for aggregation:
		* SploutSQL
		* Voldemort (read-only)
		* HBase (bulk loading)
		* Druid
		* [LinkedIn Pinot](https://github.com/linkedin/pinot)?: columnar DB for single-table SQL queries
			* gets SQL through [sql4d](https://github.com/srikalyc/Sql4D)
		* ElephantDB
		* Riak

Container/cluster management: -- **Docker + Kubernetes**
- [Docker](https://www.docker.com/): containers
	- Libs [around Docker](https://www.mindmeister.com/389671722/docker-ecosystem)
		- [Dokku](https://github.com/progrium/dokku/): Docker powered mini-Heroku in around 100 lines of Bash
		- [Flocker](https://github.com/clusterhq/flocker): manage cluster Docker containers & their data
		- [Docker Compose](https://docs.docker.com/compose/yml/): define a multi-container application in a single file
	- Managers:
	- [Docker Swarm](https://github.com/docker/swarm): manage Docker over cluster
		- Docker API so compatible with Docker tools: Dokku, Compose, Krane, Flynn, Deis, DockerUI, Shipyard, Drone, Jenkins
	- [kubernetes](http://kubernetes.io/): manage Docker swarms (on AWS, Vagrant or w/e)
		- [examples](https://github.com/GoogleCloudPlatform/kubernetes/tree/master/examples) incl. [Spark](https://github.com/GoogleCloudPlatform/kubernetes/tree/master/examples/spark), Cassandra, Celery-RabbitMQ, Meteor, WordPress, Redis, RethinkDB, Storm, [Kafka](https://github.com/graemej/k8s-kafka)
		- [kubernetes-mesos](https://github.com/mesosphere/kubernetes-mesos): manage containers (+ resources) in cluster
	- [Weave](https://github.com/weaveworks/weave): virtual network connecting Docker containers across cluster
	- [Marathon](http://mesosphere.github.io/marathon/): cluster-wide init and control system for services in cgroups or Docker containers, using [Chronos](https://github.com/mesos/chronos) for scheduling
	- [Apache Aurora](http://aurora.incubator.apache.org/), similar
	- [Hubspot's Singularity](https://github.com/HubSpot/Singularity), similar

'Operating systems': -- **DCOS on CoreOS**
- [Core OS](https://coreos.com/) (etc/fleet): like DCOS, though with more mentions of Kubernetes, less of Apache/Hadoop stuff
	- [CloudFormation templates](https://coreos.com/docs/running-coreos/cloud-providers/ec2/)
	- [Tectonic](https://tectonic.com/): Kubernetes hosting service on CoreOS
	- [Pachyderm](http://www.pachyderm.io/): MapReduce with Docker (on CoreOS clusters) instead of Hadoop
- [Apache Mesos](http://mesos.apache.org/): Program against your datacenter like it’s a single pool of resources
	- [Mesosphere Datacenter Operating System](https://mesosphere.com/) (DCOS): cluster deployment/management
		- [interfaces with](https://github.com/mesosphere) Kafka, Spark, ...
			- [custom spark](https://mesosphere.com/blog/2015/06/15/meet-a-new-version-of-spark-built-just-for-the-dcos/) but no details found, asked on Twitter now
		- [CloudFormation templates](https://docs.mesosphere.com/getting-started/cloud/amazon/) -> [Early Access Program](http://beta-docs.mesosphere.com/install/eapaws/)
		- [Vagrant](https://docs.mesosphere.com/getting-started/developer/vm-install/) version [playa-mesos](https://github.com/mesosphere/playa-mesos)
			- [Mesos UI](http://10.141.141.10:5050/)
			- [Marathon UI](http://10.141.141.10:8080/)
		- works on [CoreOS](https://docs.mesosphere.com/tutorials/mesosphere-on-a-single-coreos-instance/)!
		- [Kubernetes on Mesos](https://docs.mesosphere.com/tutorials/kubernetes-mesos-gcp/) guide

Interface with AWS:
- use custom AMIs
- [boto](https://github.com/boto/boto): Python + AWS
- [ec2_tools](https://github.com/mnielsen/ec2_tools): boto wrapper with Fabric integration
	- [Fabric](http://www.fabfile.org/): run commands on a cluster over SSH
- use EC2 spot instances for 90% cost reduction
- Stealthly [Minotaur](https://github.com/stealthly/minotaur): AWS CloudFormation stuff
- [Salt](http://saltstack.com/community/): infrastructure management (like [Chef](http://www.opscode.com/chef/), Ansible, [Puppet](http://puppetlabs.com/)?)
- Netflix [Bakery4AWS](http://bakery.answersforaws.com/), [Asgard](https://github.com/Netflix/asgard)
- Netflix [Aminator](https://github.com/netflix/aminator)
	- With Mesosphere, only one base image [AMI] is required for the entire cluster. Mesosphere manages the deployment of application code on top of the base image. ([link](https://docs.mesosphere.com/benefits-cloud/))

	Cloud abstraction: -- LibCloud, CloudBreak?
	* [Apache LibCloud](https://github.com/apache/libcloud): allows using existing AMIs
	* [Apache JClouds](https://github.com/jclouds/jclouds)
	* [Apache DeltaCloud](https://github.com/apache/deltacloud)
	* Hadoop: [CloudBreak](https://github.com/sequenceiq/cloudbreak)

Spark Notebook:
* [Hue 3.8](http://gethue.com/category/hue-3-8/)'s notebook/Spark UI
	* tried installing on [CDH](www.cloudera.com/content/cloudera/en/documentation/core/latest/topics/cdh_ig_hue_upgrade.html) and [HDP](http://gethue.com/hadoop-hue-3-on-hdp-installation-tutorial/), neither succeeded
* Apache [Zeppelin](https://zeppelin.incubator.apache.org/)
	* [Docker](https://registry.hub.docker.com/u/epahomov/docker-zeppelin/)
	* kept getting interpreter errors... possibly fixed in master
* [Spark Notebook](https://registry.hub.docker.com/u/andypetrella/spark-notebook/) - flexible [Docker](https://registry.hub.docker.com/u/andypetrella/spark-notebook/) images
* [Spark Jobserver](https://github.com/spark-jobserver/spark-jobserver)
	* [Docker](https://registry.hub.docker.com/u/omriiluz/spark-jobserver/)
	* [Docker](https://registry.hub.docker.com/u/theclaymethod/spark-jobserver-mesos/)
* [iSpark](https://github.com/tribbloid/ISpark)
	* [Docker](https://registry.hub.docker.com/u/cineca/spark-ipython/)?
* iScala / [sparknotebook](https://github.com/hohonuuli/sparknotebook)
* iPython Notebook (Jupyter)

DAX-to-SQL / SQL Server DirectQuery:
* [SSRS](http://localhost/ReportServer)
* [Formula Compatibility in DirectQuery Mode](https://msdn.microsoft.com/en-us/library/hh213006.aspx)
* [ssrs thru SSDT, which failed to install](https://www.simple-talk.com/sql/database-administration/using-dax-to-create-ssrs-reports-the-basics/)
* [ssas with ssdt](http://www.mssqltips.com/sqlservertip/2934/directquery-mode-in-sql-server-2012-analysis-services-ssas-tabular/)
* [ssrs thru SSDT](http://cathydumas.com/2012/03/20/using-reporting-services-with-directquery-models/)
* [ssdt, which failed to install](http://www.mssqlgirl.com/hybrid-mode-in-tabular-bi-semantic-model-part-1.html)
* [ssas project from ssms (management studio), but cannot open .smproj without I guess SSDT, nor sln in VS](http://blogs.msdn.com/b/cathyk/archive/2011/12/13/directquery-impersonation-options-explained.aspx)
* SSDT install: VS Tools -> Extentions -> SQL Server blah download: succeeded?
* SSDT-BI install: errors "A valid destination folder for the install could not be determined." due to some missing e:\sql12_main_t\sql\setup\darwin\sqlcastub\catarget.cpp

Node:

- [Node.io](https://www.npmjs.com/package/node.io): 40k fetches in few seconds on EC2 micro
	- proxy rotation: `npm install requester`
	- set callback with rotating proxy
	- ditto for User Agent: run with `--spoof`
- [request, cheerio and async](https://github.com/chriso/node.io)
	- [request](https://github.com/mikeal/request): fetch
	- [cheerio](https://github.com/MatthewMueller/cheerio): jQuery for Node
	- [async](https://github.com/caolan/async): parallelize
- [noodle](http://noodlejs.com/): json-style extraction templates
- [Osmosis](https://github.com/rc0x03/node-osmosis): scraper/parser; succinct though unsure of distribution/politeness...


Scala:

- [spookystuff](https://github.com/tribbloid/spookystuff) -- focuses on browser interaction
- [scalescrape](https://github.com/bfil/scalescrape) -- focuses on crap like login, pressing stuff


Parallel + Curl:

- pass db query results through Python
	- :warning: this beat the point of using Shell in the first place, just do the scrape using scrapy-redis right away.
- calling shell commands:
	- Python:
			from subprocess import call
			`call(["ls", "-l"])`
	- Scala?
		- Hadoop-agnostic.
		- `val result = "ls -al" #| "grep Foo" !!`
	- Spark DataFrame?
		- `sqlContext.sql("FROM src SELECT key, value").collect()`
		- `df.col(name)`
	- Spark: sc.addFile(path); rdd.pipe()

Links:
* http://hadoopecosystemtable.github.io/
* https://github.com/zenkay/bigdata-ecosystem/
* https://github.com/izenecloud/big-data-made-easy
