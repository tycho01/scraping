from tasks import add
result = add.delay(4, 4)
result.ready()
result.get(timeout=1)
result.get(propagate=False)
result.traceback

from celery import group
from proj.tasks import add
group(add.s(i, i) for i in xrange(10))().get()
