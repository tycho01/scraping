sudo apt-get install rabbitmq-server
pip install celery

celery help
celery worker --help
celery -A tasks worker -l info
celery multi start w1 -A proj -l info
celery multi restart w1 -A proj -l info
celery multi stop w1 -A proj -l info
celery multi stopwait w1 -A proj -l info
mkdir -p /var/run/celery
mkdir -p /var/log/celery
celery multi start w1 -A proj -l info --pidfile=/var/run/celery/%n.pid --logfile=/var/log/celery/%n%I.log
celery multi start 10 -A proj -l info -Q:1-3 images,video -Q:4,5 data -Q default -L:4,5 debug

# daemon:
http://docs.celeryproject.org/en/latest/tutorials/daemonizing.html#daemonizing
Init script: celeryd
Usage:	/etc/init.d/celeryd {start|stop|restart|status}
Configuration file:
/etc/default/celeryd

# options:
http://docs.celeryproject.org/en/latest/configuration.html#configuration

# monitoring:
http://docs.celeryproject.org/en/latest/userguide/monitoring.html#flower-real-time-celery-web-monitor

# workflow primitives:
https://celery.readthedocs.org/en/latest/userguide/canvas.html

* group: takes a list of tasks to apply in parallel.
* chain: link functions to call in a chain (after another).
* cord: like a group but with a 'body' callback for when group is done.
* map: task.map([1, 2]) -> [task(1), task(2)]
* starmap: like map except the arguments are applied as *args: add.starmap([(2, 2), (4, 4)]) -> [add(2, 2), add(4, 4)]
* chunks: like map but split into parts; add.chunks(big_array, 10) -> task chunks of 10 items each
