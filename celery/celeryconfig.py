# syntax check: python -m celeryconfig

import os
import sys
sys.path.insert(0, os.getcwd())

BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_RESULT_BACKEND = 'amqp://'
CELERY_DISABLE_RATE_LIMITS = True
CELERY_TASK_RESULT_EXPIRES = 30 * 60

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT=['json']
CELERY_TIMEZONE = 'Europe/Oslo'
CELERY_ENABLE_UTC = True

CELERY_ROUTES = {
    'tasks.add': 'low-priority',
}
CELERY_ANNOTATIONS = {
    'tasks.add': {'rate_limit': '10/m'}
}

CELERY_IMPORTS = ('tasks', )
