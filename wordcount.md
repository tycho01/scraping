# Hadoop (batch)

## [Java](http://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html)

```java
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        context.write(word, one);
      }
    }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(WordCount.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
```

# Storm (streaming)

## [Java](https://github.com/nathanmarz/storm-starter/blob/master/src/jvm/storm/starter/WordCountTopology.java)

```java
package storm.starter;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.task.ShellBolt;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import storm.starter.spout.RandomSentenceSpout;

import java.util.HashMap;
import java.util.Map;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */
public class WordCountTopology {
  public static class SplitSentence extends ShellBolt implements IRichBolt {

    public SplitSentence() {
      super("python", "splitsentence.py");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
      return null;
    }
  }

  public static class WordCount extends BaseBasicBolt {
    Map<String, Integer> counts = new HashMap<String, Integer>();

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
      String word = tuple.getString(0);
      Integer count = counts.get(word);
      if (count == null)
        count = 0;
      count++;
      counts.put(word, count);
      collector.emit(new Values(word, count));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word", "count"));
    }
  }

  public static void main(String[] args) throws Exception {

    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("spout", new RandomSentenceSpout(), 5);

    builder.setBolt("split", new SplitSentence(), 8).shuffleGrouping("spout");
    builder.setBolt("count", new WordCount(), 12).fieldsGrouping("split", new Fields("word"));

    Config conf = new Config();
    conf.setDebug(true);


    if (args != null && args.length > 0) {
      conf.setNumWorkers(3);

      StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
    }
    else {
      conf.setMaxTaskParallelism(3);

      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("word-count", conf, builder.createTopology());

      Thread.sleep(10000);

      cluster.shutdown();
    }
  }
}
```

## [Clojure](https://github.com/apache/storm/blob/master/examples/storm-starter/src/clj/storm/starter/clj/word_count.clj)

```clojure
(ns storm.starter.clj.word-count
  (:import [backtype.storm StormSubmitter LocalCluster])
  (:use [backtype.storm clojure config])
  (:gen-class))

(defspout sentence-spout ["sentence"]
  [conf context collector]
  (let [sentences ["a little brown dog"
                   "the man petted the dog"
                   "four score and seven years ago"
                   "an apple a day keeps the doctor away"]]
    (spout
     (nextTuple []
       (Thread/sleep 100)
       (emit-spout! collector [(rand-nth sentences)])
       )
     (ack [id]
        ;; You only need to define this method for reliable spouts
        ;; (such as one that reads off of a queue like Kestrel)
        ;; This is an unreliable spout, so it does nothing here
        ))))

(defspout sentence-spout-parameterized ["word"] {:params [sentences] :prepare false}
  [collector]
  (Thread/sleep 500)
  (emit-spout! collector [(rand-nth sentences)]))

(defbolt split-sentence ["word"] [tuple collector]
  (let [words (.split (.getString tuple 0) " ")]
    (doseq [w words]
      (emit-bolt! collector [w] :anchor tuple))
    (ack! collector tuple)
    ))

(defbolt word-count ["word" "count"] {:prepare true}
  [conf context collector]
  (let [counts (atom {})]
    (bolt
     (execute [tuple]
       (let [word (.getString tuple 0)]
         (swap! counts (partial merge-with +) {word 1})
         (emit-bolt! collector [word (@counts word)] :anchor tuple)
         (ack! collector tuple)
         )))))

(defn mk-topology []

  (topology
   {"1" (spout-spec sentence-spout)
    "2" (spout-spec (sentence-spout-parameterized
                     ["the cat jumped over the door"
                      "greetings from a faraway land"])
                     :p 2)}
   {"3" (bolt-spec {"1" :shuffle "2" :shuffle}
                   split-sentence
                   :p 5)
    "4" (bolt-spec {"3" ["word"]}
                   word-count
                   :p 6)}))

(defn run-local! []
  (let [cluster (LocalCluster.)]
    (.submitTopology cluster "word-count" {TOPOLOGY-DEBUG true} (mk-topology))
    (Thread/sleep 10000)
    (.shutdown cluster)
    ))

(defn submit-topology! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology)))

(defn -main
  ([]
   (run-local!))
  ([name]
   (submit-topology! name)))
```

## [Scala](https://github.com/velvia/ScalaStorm/blob/master/src/test/scala/storm/scala/examples/WordCountTopology.scala) (thru [ScalaStorm](https://github.com/velvia/ScalaStorm/))

```scala
package storm.scala.examples

import storm.scala.dsl._
import backtype.storm.Config
import backtype.storm.LocalCluster
import backtype.storm.topology.TopologyBuilder
import backtype.storm.tuple.{Fields, Tuple, Values}
import collection.mutable.{Map, HashMap}
import util.Random
import scala.language.postfixOps


class RandomSentenceSpout extends StormSpout(outputFields = List("sentence")) {
  val sentences = List("the cow jumped over the moon",
                       "an apple a day keeps the doctor away",
                       "four score and seven years ago",
                       "snow white and the seven dwarfs",
                       "i am at two with nature")
  def nextTuple = {
    Thread sleep 100
    emit (sentences(Random.nextInt(sentences.length)))
  }
}


// An example of using matchSeq for Scala pattern matching of Storm tuples
// plus using the emit and ack DSLs.
class SplitSentence extends StormBolt(outputFields = List("word")) {
  def execute(t: Tuple) = t matchSeq {
    case Seq(sentence: String) => sentence split " " foreach
      { word => using anchor t emit (word) }
    t ack
  }
}


class WordCount extends StormBolt(List("word", "count")) {
  var counts: Map[String, Int] = _
  setup {
    counts = new HashMap[String, Int]().withDefaultValue(0)
  }
  def execute(t: Tuple) = t matchSeq {
    case Seq(word: String) =>
      counts(word) += 1
      using anchor t emit (word, counts(word))
      t ack
  }
}


object WordCountTopology {
  def main(args: Array[String]) = {
    val builder = new TopologyBuilder

    builder.setSpout("randsentence", new RandomSentenceSpout, 5)
    builder.setBolt("split", new SplitSentence, 8)
        .shuffleGrouping("randsentence")
    builder.setBolt("count", new WordCount, 12)
        .fieldsGrouping("split", new Fields("word"))

    val conf = new Config
    conf.setDebug(true)
    conf.setMaxTaskParallelism(3)

    val cluster = new LocalCluster
    cluster.submitTopology("word-count", conf, builder.createTopology)
    Thread sleep 10000
    cluster.shutdown
  }
}
```

# Storm Trident (streaming)

## [Java](https://github.com/nathanmarz/storm-starter/blob/master/src/jvm/storm/starter/trident/TridentWordCount.java)

```java
package storm.scala.examples

import storm.scala.dsl._
import backtype.storm.Config
import backtype.storm.LocalCluster
import backtype.storm.topology.TopologyBuilder
import backtype.storm.tuple.{Fields, Tuple, Values}
import collection.mutable.{Map, HashMap}
import util.Random
import scala.language.postfixOps


class RandomSentenceSpout extends StormSpout(outputFields = List("sentence")) {
  val sentences = List("the cow jumped over the moon",
                       "an apple a day keeps the doctor away",
                       "four score and seven years ago",
                       "snow white and the seven dwarfs",
                       "i am at two with nature")
  def nextTuple = {
    Thread sleep 100
    emit (sentences(Random.nextInt(sentences.length)))
  }
}


// An example of using matchSeq for Scala pattern matching of Storm tuples
// plus using the emit and ack DSLs.
class SplitSentence extends StormBolt(outputFields = List("word")) {
  def execute(t: Tuple) = t matchSeq {
    case Seq(sentence: String) => sentence split " " foreach
      { word => using anchor t emit (word) }
    t ack
  }
}


class WordCount extends StormBolt(List("word", "count")) {
  var counts: Map[String, Int] = _
  setup {
    counts = new HashMap[String, Int]().withDefaultValue(0)
  }
  def execute(t: Tuple) = t matchSeq {
    case Seq(word: String) =>
      counts(word) += 1
      using anchor t emit (word, counts(word))
      t ack
  }
}


object WordCountTopology {
  def main(args: Array[String]) = {
    val builder = new TopologyBuilder

    builder.setSpout("randsentence", new RandomSentenceSpout, 5)
    builder.setBolt("split", new SplitSentence, 8)
        .shuffleGrouping("randsentence")
    builder.setBolt("count", new WordCount, 12)
        .fieldsGrouping("split", new Fields("word"))

    val conf = new Config
    conf.setDebug(true)
    conf.setMaxTaskParallelism(3)

    val cluster = new LocalCluster
    cluster.submitTopology("word-count", conf, builder.createTopology)
    Thread sleep 10000
    cluster.shutdown
  }
}
```

## [Scala](https://github.com/velvia/ScalaStorm/blob/master/src/test/scala/storm/scala/examples/trident/TridentWordCount.scala) (thru ScalaStorm)

```scala
package storm.scala.examples.trident

import collection.JavaConversions._

import storm.trident.tuple.TridentTuple
import storm.trident.operation.{TridentCollector, BaseFunction}
import backtype.storm.tuple.{Fields, Values}
import storm.trident.TridentTopology
import storm.trident.testing.{MemoryMapState, FixedBatchSpout}
import backtype.storm.{StormSubmitter, LocalDRPC, LocalCluster, Config}
import storm.trident.operation.builtin._
import storm.trident.state.{State, QueryFunction}

import storm.scala.dsl.FunctionalTrident._

object TridentWordCount extends App {
  def buildTopology(drpc: LocalDRPC) = {
    val spout = new FixedBatchSpout(new Fields("sentence"), 3,
                new Values("the cow jumped over the moon"),
                new Values("the man went to the store and bought some candy"),
                new Values("four score and seven years ago"),
                new Values("how many apples can you eat"),
                new Values("to be or not to be the person"));
    spout.setCycle(true);

    val topology = new TridentTopology();
    val wordCounts = topology.newStream("spout1", spout)
      .parallelismHint(16)
      .flatMap("sentence" -> "word") { _.getString(0).split(" ") }
      .groupBy(new Fields("word"))
      .persistentAggregate(new MemoryMapState.Factory(), new Count(), new Fields("count"))

    topology.newDRPCStream("words", drpc)
        .flatMap("args" -> "word") { _.getString(0).split(" ") }
        .groupBy(new Fields("word"))
        .stateQuery(wordCounts, new Fields("word"),
                    // Unfortunately asInstanceOf[] needed to get around an apparent scalac bug
                    (new MapGet).asInstanceOf[QueryFunction[_ <: State, _]], new Fields("count"))
        .each(new Fields("count"), new FilterNull())
        .aggregate(new Fields("count"), new Sum(), new Fields("sum"))

    topology.build()
  }

  val conf = new Config
  conf.setMaxSpoutPending(20)
  val cluster = new LocalCluster
  if (args.length == 0) {
    val drpc = new LocalDRPC
    cluster.submitTopology("wordCounter", conf, buildTopology(drpc))
    (0 to 100).foreach { i =>
      println("DRPC RESULT: " + drpc.execute("words", "cat the dog jumped"))
      Thread sleep 1000
    }
  } else {
    cluster.submitTopology(args(0), conf, buildTopology(null));
  }
}
```

## [Clojure](https://github.com/whoahbot/trident-in-clojure/blob/master/src/main/clojure/trident_in_clojure/core.clj) (thru [trident-in-clojure](https://github.com/whoahbot/trident-in-clojure/))

```clojure
(ns trident-in-clojure.core
  "CLI / server bootstrap."
  (:import [backtype.storm Config StormSubmitter LocalCluster LocalDRPC]
           [backtype.storm.spout SchemeAsMultiScheme]
           [backtype.storm.tuple Fields Tuple]
           [storm.trident TridentTopology]
           [storm.trident.tuple TridentTupleView]
           [storm.trident.testing MemoryMapState$Factory]
           [storm.trident.operation.builtin Count Sum MapGet FilterNull]
           [storm.kafka.trident OpaqueTridentKafkaSpout
            TridentKafkaConfig]
           [storm.kafka SpoutConfig KafkaConfig$ZkHosts StringScheme])
  (:require [trident-in-clojure.word-splitter]
            [clojure.tools.logging :as log])
  (:use [backtype.storm clojure config])
  (:gen-class))

(def config
  {:topic "words"
   :kafka {:zookeeper "127.0.0.1:2181"
           :broker-path "/brokers"}})

(defn create-kafka-spout
  [{:keys [kafka topic]}]
  (let [{:keys [zookeeper broker-path]} kafka
        zk-hosts (KafkaConfig$ZkHosts. zookeeper broker-path)
        spout-config (TridentKafkaConfig. zk-hosts topic)]
    (set! (.scheme spout-config) (SchemeAsMultiScheme. (StringScheme.)))
    (OpaqueTridentKafkaSpout. spout-config)))

(defn mk-drpc-stream
  [drpc topology wordcounts]
  (-> (.newDRPCStream topology "words" drpc)
      (.each (Fields. ["args"]) (com.whoahbot.trident.WordSplitter.) (Fields. ["word"]))
      (.groupBy (Fields. ["word"]))
      (.stateQuery wordcounts (Fields. ["word"])
                   (storm.trident.operation.builtin.MapGet.) (Fields. ["count"]))
      (.each (Fields. ["count"]) (FilterNull.))
      (.aggregate (Fields. ["count"]) (Sum.) (Fields. ["sum"]))))

(defn mk-wordcount-topology
  [topology config]
  (-> topology
      (.newStream "word-spout"
                  (create-kafka-spout config))
      (.each (Fields. ["str"])
             (com.whoahbot.trident.WordSplitter.)
             (Fields. ["word"]))
      (.groupBy (Fields. ["word"]))
      (.persistentAggregate (MemoryMapState$Factory.) (Count.) (Fields. ["count"]))))

(defn run-local-topology
  [config drpc]
  (let [cluster (LocalCluster.)
        cluster-config (Config.)
        topology (TridentTopology.)
        wordcounts (mk-wordcount-topology topology config)]
    (mk-drpc-stream drpc topology wordcounts)
    ;;(.setDebug cluster-config true)
    (.submitTopology cluster "wordcount-local-topology"
                     cluster-config
                     (.build topology))))

(defn -main
  [& args]
  (let [drpc (LocalDRPC.)]
    (run-local-topology config drpc)
    (while true
      (log/infof "Word count: %s" (.execute drpc "words" "baby"))
      (Thread/sleep 1000))))
```

# Samza (streaming)

## [Java](http://samza.apache.org/learn/documentation/0.7.0/api/overview.html)

```java
public class SplitStringIntoWords implements StreamTask {

  // Send outgoing messages to a stream called "words"
  // in the "kafka" system.
  private final SystemStream OUTPUT_STREAM =
    new SystemStream("kafka", "words");

  public void process(IncomingMessageEnvelope envelope,
                      MessageCollector collector,
                      TaskCoordinator coordinator) {
    String message = (String) envelope.getMessage();

    for (String word : message.split(" ")) {
      // Use the word as the key, and 1 as the value.
      // A second task can add the 1's to get the word count.
      collector.send(new OutgoingMessageEnvelope(OUTPUT_STREAM, word, 1));
    }
  }
}
```

# Pig (batch)

## [Pig Latin](http://hortonworks.com/hadoop-tutorial/word-counting-with-apache-pig/)

```pig
a = load '/user/hue/word_count_text.txt';
b = foreach a generate flatten(TOKENIZE((chararray)$0)) as word;
c = group b by word;
d = foreach c generate COUNT(b), group;
store d into '/user/hue/pig_wordcount';
```

# Hive (batch)

## [HiveQL](http://stackoverflow.com/questions/10039949/word-count-program-in-hive)

```hive
SELECT word, COUNT(*) FROM input LATERAL VIEW explode(split(text, ' ')) lTable as word GROUP BY word;
```

# Cascading (batch) -- [slow?](http://pangool.net/benchmark.html)

## [Java](https://github.com/Cascading/cascading.samples/blob/master/wordcount/src/java/wordcount/Main.java)

```java
package wordcount;

import java.util.Map;
import java.util.Properties;

import cascading.cascade.Cascade;
import cascading.cascade.CascadeConnector;
import cascading.cascade.Cascades;
import cascading.flow.Flow;
import cascading.flow.FlowConnector;
import cascading.flow.hadoop.HadoopFlowConnector;
import cascading.operation.Identity;
import cascading.operation.aggregator.Count;
import cascading.operation.regex.RegexFilter;
import cascading.operation.regex.RegexGenerator;
import cascading.operation.regex.RegexReplace;
import cascading.operation.regex.RegexSplitter;
import cascading.operation.xml.TagSoupParser;
import cascading.operation.xml.XPathGenerator;
import cascading.operation.xml.XPathOperation;
import cascading.pipe.Each;
import cascading.pipe.Every;
import cascading.pipe.GroupBy;
import cascading.pipe.Pipe;
import cascading.pipe.SubAssembly;
import cascading.property.AppProps;
import cascading.scheme.hadoop.SequenceFile;
import cascading.scheme.hadoop.TextLine;
import cascading.tap.Tap;
import cascading.tap.hadoop.Hfs;
import cascading.tap.hadoop.Lfs;
import cascading.tuple.Fields;

public class Main
  {

  private static class ImportCrawlDataAssembly extends SubAssembly
    {
    public ImportCrawlDataAssembly( String name )
      {
      // split the text line into "url" and "raw" with the default delimiter of tab
      RegexSplitter regexSplitter = new RegexSplitter( new Fields( "url", "raw" ) );
      Pipe importPipe = new Each( name, new Fields( "line" ), regexSplitter );
      // remove all pdf documents from the stream
      importPipe = new Each( importPipe, new Fields( "url" ), new RegexFilter( ".*\\.pdf$", true ) );
      // replace ":nl" with a new line, return the fields "url" and "page" to the stream.
      // discard the other fields in the stream
      RegexReplace regexReplace = new RegexReplace( new Fields( "page" ), ":nl:", "\n" );
      importPipe = new Each( importPipe, new Fields( "raw" ), regexReplace, new Fields( "url", "page" ) );

      setTails( importPipe );
      }
    }

  private static class WordCountSplitAssembly extends SubAssembly
    {
    public WordCountSplitAssembly( String sourceName, String sinkUrlName, String sinkWordName )
      {
      // create a new pipe assembly to create the word count across all the pages, and the word count in a single page
      Pipe pipe = new Pipe( sourceName );

      // convert the html to xhtml using the TagSouParser. return only the fields "url" and "xml", discard the rest
      pipe = new Each( pipe, new Fields( "page" ), new TagSoupParser( new Fields( "xml" ) ), new Fields( "url", "xml" ) );
      // apply the given XPath expression to the xml in the "xml" field. this expression extracts the 'body' element.
      XPathGenerator bodyExtractor = new XPathGenerator( new Fields( "body" ), XPathOperation.NAMESPACE_XHTML, "//xhtml:body" );
      pipe = new Each( pipe, new Fields( "xml" ), bodyExtractor, new Fields( "url", "body" ) );
      // apply another XPath expression. this expression removes all elements from the xml, leaving only text nodes.
      // text nodes in a 'script' element are removed.
      String elementXPath = "//text()[ name(parent::node()) != 'script']";
      XPathGenerator elementRemover = new XPathGenerator( new Fields( "words" ), XPathOperation.NAMESPACE_XHTML, elementXPath );
      pipe = new Each( pipe, new Fields( "body" ), elementRemover, new Fields( "url", "words" ) );
      // apply the regex to break the document into individual words and stuff each word at a new tuple into the current
      // stream with field names "url" and "word"
      RegexGenerator wordGenerator = new RegexGenerator( new Fields( "word" ), "(?<!\\pL)(?=\\pL)[^ ]*(?<=\\pL)(?!\\pL)" );
      pipe = new Each( pipe, new Fields( "words" ), wordGenerator, new Fields( "url", "word" ) );

      // group on "url"
      Pipe urlCountPipe = new GroupBy( sinkUrlName, pipe, new Fields( "url", "word" ) );
      urlCountPipe = new Every( urlCountPipe, new Fields( "url", "word" ), new Count(), new Fields( "url", "word", "count" ) );

      // group on "word"
      Pipe wordCountPipe = new GroupBy( sinkWordName, pipe, new Fields( "word" ) );
      wordCountPipe = new Every( wordCountPipe, new Fields( "word" ), new Count(), new Fields( "word", "count" ) );

      setTails( urlCountPipe, wordCountPipe );
      }
    }

  public static void main( String[] args )
    {
    // set the current job jar
    Properties properties = new Properties();
    AppProps.setApplicationJarClass( properties, Main.class );
    FlowConnector flowConnector = new HadoopFlowConnector( properties );

    String inputPath = args[ 0 ];
    String pagesPath = args[ 1 ] + "/pages/";
    String urlsPath = args[ 1 ] + "/urls/";
    String wordsPath = args[ 1 ] + "/words/";
    String localUrlsPath = args[ 2 ] + "/urls/";
    String localWordsPath = args[ 2 ] + "/words/";

    // import a text file with crawled pages from the local filesystem into a Hadoop distributed filesystem
    // the imported file will be a native Hadoop sequence file with the fields "page" and "url"
    // note this examples stores crawl pages as a tabbed file, with the first field being the "url"
    // and the second being the "raw" document that had all new line chars ("\n") converted to the text ":nl:".

    // a predefined pipe assembly that returns fields named "url" and "page"
    Pipe importPipe = new ImportCrawlDataAssembly( "import pipe" );

    // create the tap instances
    Tap localPagesSource = new Lfs( new TextLine(), inputPath );
    Tap importedPages = new Hfs( new SequenceFile( new Fields( "url", "page" ) ), pagesPath );

    // connect the pipe assembly to the tap instances
    Flow importPagesFlow = flowConnector.connect( "import pages", localPagesSource, importedPages, importPipe );

    // a predefined pipe assembly that splits the stream into two named "url pipe" and "word pipe"
    // these pipes could be retrieved via the getTails() method and added to new pipe instances
    SubAssembly wordCountPipe = new WordCountSplitAssembly( "wordcount pipe", "url pipe", "word pipe" );

    // create Hadoop sequence files to store the results of the counts
    Tap sinkUrl = new Hfs( new SequenceFile( new Fields( "url", "word", "count" ) ), urlsPath );
    Tap sinkWord = new Hfs( new SequenceFile( new Fields( "word", "count" ) ), wordsPath );

    // convenience method to bind multiple pipes and taps
    Map<String, Tap> sinks = Cascades.tapsMap( new String[]{"url pipe", "word pipe"}, Tap.taps( sinkUrl, sinkWord ) );

    // wordCountPipe will be recognized as an assembly and handled appropriately
    Flow count = flowConnector.connect( importedPages, sinks, wordCountPipe );

    // create an assembly to export the Hadoop sequence file to local text files
    Pipe exportPipe = new Each( "export pipe", new Identity() );

    Tap localSinkUrl = new Lfs( new TextLine(), localUrlsPath );
    Tap localSinkWord = new Lfs( new TextLine(), localWordsPath );

    // connect up both sinks using the same exportPipe assembly
    Flow exportFromUrl = flowConnector.connect( "export url", sinkUrl, localSinkUrl, exportPipe );
    Flow exportFromWord = flowConnector.connect( "export word", sinkWord, localSinkWord, exportPipe );

    // connect up all the flows, order is not significant
    Cascade cascade = new CascadeConnector().connect( importPagesFlow, count, exportFromUrl, exportFromWord );

    // run the cascade to completion
    cascade.complete();
    }
  }
```

# Scalding

## [Scala](https://github.com/twitter/scalding/wiki/Getting-Started)

```scala
import com.twitter.scalding._

class WordCountJob(args : Args) extends Job(args) {
  TypedPipe.from(TextLine(args("input")))
    .flatMap { line => line.split("""\s+""") }
    .groupBy { word => word }
    .size
    .write(TypedTsv(args("output")))
}
```

# Cascalog (batch)

## [Clojure](https://github.com/sritchie/cascalog-class/blob/master/src/cascalog_class/core.clj)

```clojure
(ns cascalog-class.core
  (:use cascalog.api)
  (:require [cascalog.ops :as c]))

(defmapcatop split
  "Accepts a sentence 1-tuple, splits that sentence on whitespace, and
   emits a single 1-tuple for each word."
  [^String sentence]
  (.split sentence "\\s+"))

(defn wordcount-query
  "Accepts a generator of lines of text and returns a subquery that
  generates a count for each word in the text sample."
  [src]
  (<- [?word ?count]
      (src ?textline)
      (split ?textline :> ?word)
      (c/count ?count)))
```

# Crunch (batch)

## [Java](http://blog.cloudera.com/blog/2011/10/introducing-crunch/)

```java
import com.cloudera.crunch.DoFn;
import com.cloudera.crunch.Emitter;
import com.cloudera.crunch.PCollection;
import com.cloudera.crunch.PTable;
import com.cloudera.crunch.Pipeline;
import com.cloudera.crunch.impl.mr.MRPipeline;
import com.cloudera.crunch.lib.Aggregate;
import com.cloudera.crunch.type.writable.Writables;

public class WordCount {
  public static void main(String[] args) throws Exception {
    // Create an object to coordinate pipeline creation and execution.
    Pipeline pipeline = new MRPipeline(WordCount.class);
    // Reference a given text file as a collection of Strings.
    PCollection<String> lines = pipeline.readTextFile(args[0]);

    // Define a function that splits each line in a PCollection of Strings into a
    // PCollection made up of the individual words in the file.
    PCollection<String> words = lines.parallelDo(new DoFn<String, String>() {
      public void process(String line, Emitter<String> emitter) {
        for (String word : line.split("\\s+")) {
          emitter.emit(word);
        }
      }
    }, Writables.strings()); // Indicates the serialization format

    // The Aggregate.count method applies a series of Crunch primitives and returns
    // a map of the unique words in the input PCollection to their counts.
    // Best of all, the count() function doesn't need to know anything about
    // the kind of data stored in the input PCollection.
    PTable<String, Long> counts = Aggregate.count(words);

    // Instruct the pipeline to write the resulting counts to a text file.
    pipeline.writeTextFile(counts, args[1]);
    // Execute the pipeline as a MapReduce.
    pipeline.done();
  }
}
```

# Scrunch (batch)

## [Scala](https://github.com/cloudera/crunch/blob/master/scrunch/src/main/examples/WordCount.scala)

```scala
import org.apache.scrunch.PipelineApp

object WordCount extends PipelineApp {

  def countWords(file: String) = {
    read(from.textFile(file))
      .flatMap(_.split("\\W+").filter(!_.isEmpty()))
      .count
  }

  val counts = join(countWords(args(0)), countWords(args(1)))
  write(counts, to.textFile(args(2)))
}
```

# Pangool (batch)

## [Java](https://github.com/datasalt/pangool-benchmark/blob/master/src/main/java/com/datasalt/pangool/benchmark/wordcount/PangoolWordCount.java)

```java
package com.datasalt.pangool.benchmark.wordcount;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.datasalt.pangool.io.ITuple;
import com.datasalt.pangool.io.Schema;
import com.datasalt.pangool.io.Tuple;
import com.datasalt.pangool.io.Schema.Field;
import com.datasalt.pangool.io.Schema.Field.Type;
import com.datasalt.pangool.tuplemr.TupleMRBuilder;
import com.datasalt.pangool.tuplemr.TupleMRException;
import com.datasalt.pangool.tuplemr.TupleMapper;
import com.datasalt.pangool.tuplemr.TupleReducer;
import com.datasalt.pangool.tuplemr.mapred.lib.input.HadoopInputFormat;
import com.datasalt.pangool.tuplemr.mapred.lib.output.HadoopOutputFormat;

/**
 * Code for solving the simple PangoolWordCount problem in Pangool.
 */
public class PangoolWordCount {

	public final static Charset UTF8 = Charset.forName("UTF-8");

	@SuppressWarnings("serial")
	public static class Split extends TupleMapper<LongWritable, Text> {

		private Tuple tuple;

		@Override
		public void map(LongWritable key, Text value, TupleMRContext context, Collector collector)
		    throws IOException, InterruptedException {
			if (tuple == null){
				tuple = new Tuple(context.getTupleMRConfig().getIntermediateSchema(0));
			}

			StringTokenizer itr = new StringTokenizer(value.toString());
			tuple.set(1, 1);
			while(itr.hasMoreTokens()) {
				tuple.set(0, itr.nextToken());
				collector.write(tuple);
			}
		}
	}

	@SuppressWarnings("serial")
	public static class CountCombiner extends TupleReducer<ITuple, NullWritable> {

		@Override
		public void reduce(ITuple group, Iterable<ITuple> tuples, TupleMRContext context, Collector collector)
		    throws IOException, InterruptedException, TupleMRException {
			int count = 0;
			ITuple outputTuple=null;
			for(ITuple tuple : tuples) {
				outputTuple = tuple;
				count += (Integer) tuple.get(1);
			}
			outputTuple.set(1, count);
			collector.write(outputTuple, NullWritable.get());
		}
	}

	@SuppressWarnings("serial")
	public static class Count extends TupleReducer<Text, IntWritable> {
		private IntWritable outputCount;

		@Override
		public void reduce(ITuple group, Iterable<ITuple> tuples, TupleMRContext context, Collector collector)
		    throws IOException, InterruptedException, TupleMRException {

			if(outputCount == null) {
				outputCount = new IntWritable();
			}
			int count = 0;
			for(ITuple tuple : tuples) {
				count += (Integer) tuple.get(1);
			}
			outputCount.set(count);
			collector.write((Text)group.get(0), outputCount);
		}
	}

	public Job getJob(Configuration conf, String input, String output) throws TupleMRException,
	    IOException {
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(output), true);


		List<Field> fields = new ArrayList<Field>();
		fields.add(Field.create("word",Type.STRING));
		fields.add(Field.create("count",Type.INT));
		Schema schema = new Schema("schema",fields);

		TupleMRBuilder cg = new TupleMRBuilder(conf,"Pangool WordCount");
		cg.addIntermediateSchema(schema);
		cg.setGroupByFields("word");
		cg.setJarByClass(PangoolWordCount.class);
		cg.addInput(new Path(input), new HadoopInputFormat(TextInputFormat.class), new Split());
		cg.setOutput(new Path(output), new HadoopOutputFormat(TextOutputFormat.class), Text.class, Text.class);
		cg.setTupleReducer(new Count());
		cg.setTupleCombiner(new CountCombiner());

		return cg.createJob();
	}

	private static final String HELP = "Usage: PangoolWordCount [input_path] [output_path]";

	public static void main(String args[]) throws TupleMRException, IOException, InterruptedException,
	    ClassNotFoundException {
		if(args.length != 2) {
			System.err.println("Wrong number of arguments");
			System.err.println(HELP);
			System.exit(-1);
		}

		Configuration conf = new Configuration();
		new PangoolWordCount().getJob(conf, args[0], args[1]).waitForCompletion(true);
	}
}
```

# Flink (batch + streaming)

## [Java (batch?)](https://github.com/apache/flink/blob/master/flink-examples/flink-java-examples/src/main/java/org/apache/flink/examples/java/wordcount/WordCount.java)

```java
package org.apache.flink.examples.java.wordcount;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.examples.java.wordcount.util.WordCountData;
import org.apache.flink.util.Collector;

@SuppressWarnings("serial")
public class WordCount {

	public static void main(String[] args) throws Exception {

		if(!parseParameters(args)) {
			return;
		}

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		DataSet<String> text = getTextDataSet(env);
		DataSet<Tuple2<String, Integer>> counts =
				text.flatMap(new Tokenizer())
				.groupBy(0)
				.sum(1);

		if(fileOutput) {
			counts.writeAsCsv(outputPath, "\n", " ");
			env.execute("WordCount Example");
		} else {
			counts.print();
		}


	}

	public static final class Tokenizer implements FlatMapFunction<String, Tuple2<String, Integer>> {

		@Override
		public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
			// normalize and split the line
			String[] tokens = value.toLowerCase().split("\\W+");

			// emit the pairs
			for (String token : tokens) {
				if (token.length() > 0) {
					out.collect(new Tuple2<String, Integer>(token, 1));
				}
			}
		}
	}

	private static boolean fileOutput = false;
	private static String textPath;
	private static String outputPath;

	private static boolean parseParameters(String[] args) {

		if(args.length > 0) {
			// parse input arguments
			fileOutput = true;
			if(args.length == 2) {
				textPath = args[0];
				outputPath = args[1];
			} else {
				System.err.println("Usage: WordCount <text path> <result path>");
				return false;
			}
		} else {
			System.out.println("Executing WordCount example with built-in default data.");
			System.out.println("  Provide parameters to read input data from a file.");
			System.out.println("  Usage: WordCount <text path> <result path>");
		}
		return true;
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env) {
		if(fileOutput) {
			// read the text file from given input path
			return env.readTextFile(textPath);
		} else {
			// get default test text data
			return WordCountData.getDefaultTextLineDataSet(env);
		}
	}
}
```

## [Java (streaming)](http://image.slidesharecdn.com/flinkstreaming-150112171000-conversion-gate01/95/flink-streaming-7-638.jpg?cb=1421082945), [1](https://github.com/apache/flink/blob/master/flink-java8/src/main/java/org/apache/flink/streaming/examples/java8/wordcount/WordCount.java), [2](https://github.com/apache/flink/tree/master/flink-quickstart/flink-quickstart-java/src/main/resources/archetype-resources/src/main/java)

```java
DataStream<String> text = env.socketTextStream(host, port);
DataStream<Tuple2<String, Integer>> result = text.flatMap((str, out) -> {
  for (String token : value.split("\\W")) {
    out.collect(new Tuple2<>(token, 1));
  })
  .groupBy(0)
  .sum(1);
```

## [Scala (streaming)](http://image.slidesharecdn.com/flinkstreaming-150112171000-conversion-gate01/95/flink-streaming-8-638.jpg?cb=1421082945), [1](https://github.com/apache/flink/blob/master/flink-examples/flink-scala-examples/src/main/scala/org/apache/flink/examples/scala/wordcount/WordCount.scala), [2](https://github.com/apache/flink/tree/master/flink-quickstart/flink-quickstart-scala/src/main/resources/archetype-resources/src/main/scala)

```scala
case class Word(word: String, count: Long)
val input = env.socketTextStream(host, port);
val words = input flatMap { line =>.split("\\W+").map(Word(_,1)) }
val counts = words groupBy "word" sum "count"
```

# Summingbird (batch + streaming)

## [Scala](https://github.com/twitter/summingbird)

```scala
def wordCount[P <: Platform[P]]
  (source: Producer[P, String], store: P#Store[String, Long]) =
    source.flatMap { sentence =>
      toWords(sentence).map(_ -> 1L)
    }.sumByKey(store)
```

# Spark (batch + streaming)

## [Python](https://github.com/apache/spark/blob/master/examples/src/main/python/wordcount.py)

```python
from __future__ import print_function

import sys
from operator import add

from pyspark import SparkContext


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: wordcount <file>", file=sys.stderr)
        exit(-1)
    sc = SparkContext(appName="PythonWordCount")
    lines = sc.textFile(sys.argv[1], 1)
    counts = lines.flatMap(lambda x: x.split(' ')).map(lambda x: (x, 1)).reduceByKey(add)
    output = counts.collect()
    for (word, count) in output:
        print("%s: %i" % (word, count))

    sc.stop()
```

## [Java (batch)](https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/JavaWordCount.java)

```java
package org.apache.spark.examples;

import scala.Tuple2;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class JavaWordCount {
  private static final Pattern SPACE = Pattern.compile(" ");

  public static void main(String[] args) throws Exception {

    if (args.length < 1) {
      System.err.println("Usage: JavaWordCount <file>");
      System.exit(1);
    }

    SparkConf sparkConf = new SparkConf().setAppName("JavaWordCount");
    JavaSparkContext ctx = new JavaSparkContext(sparkConf);
    JavaRDD<String> lines = ctx.textFile(args[0], 1);

    JavaRDD<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
      @Override
      public Iterable<String> call(String s) {
        return Arrays.asList(SPACE.split(s));
      }
    });

    JavaPairRDD<String, Integer> ones = words.mapToPair(new PairFunction<String, String, Integer>() {
      @Override
      public Tuple2<String, Integer> call(String s) {
        return new Tuple2<String, Integer>(s, 1);
      }
    });

    JavaPairRDD<String, Integer> counts = ones.reduceByKey(new Function2<Integer, Integer, Integer>() {
      @Override
      public Integer call(Integer i1, Integer i2) {
        return i1 + i2;
      }
    });

    List<Tuple2<String, Integer>> output = counts.collect();
    for (Tuple2<?,?> tuple : output) {
      System.out.println(tuple._1() + ": " + tuple._2());
    }
    ctx.stop();
  }
}
```

## [Java + Kafka (streaming)](https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/streaming/JavaKafkaWordCount.java)

```java
package org.apache.spark.examples.streaming;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;

import scala.Tuple2;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.examples.streaming.StreamingExamples;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

public final class JavaKafkaWordCount {
  private static final Pattern SPACE = Pattern.compile(" ");

  private JavaKafkaWordCount() {
  }

  public static void main(String[] args) {
    if (args.length < 4) {
      System.err.println("Usage: JavaKafkaWordCount <zkQuorum> <group> <topics> <numThreads>");
      System.exit(1);
    }

    StreamingExamples.setStreamingLogLevels();
    SparkConf sparkConf = new SparkConf().setAppName("JavaKafkaWordCount");
    // Create the context with a 1 second batch size
    JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, new Duration(2000));

    int numThreads = Integer.parseInt(args[3]);
    Map<String, Integer> topicMap = new HashMap<String, Integer>();
    String[] topics = args[2].split(",");
    for (String topic: topics) {
      topicMap.put(topic, numThreads);
    }

    JavaPairReceiverInputDStream<String, String> messages =
            KafkaUtils.createStream(jssc, args[0], args[1], topicMap);

    JavaDStream<String> lines = messages.map(new Function<Tuple2<String, String>, String>() {
      @Override
      public String call(Tuple2<String, String> tuple2) {
        return tuple2._2();
      }
    });

    JavaDStream<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
      @Override
      public Iterable<String> call(String x) {
        return Lists.newArrayList(SPACE.split(x));
      }
    });

    JavaPairDStream<String, Integer> wordCounts = words.mapToPair(
      new PairFunction<String, String, Integer>() {
        @Override
        public Tuple2<String, Integer> call(String s) {
          return new Tuple2<String, Integer>(s, 1);
        }
      }).reduceByKey(new Function2<Integer, Integer, Integer>() {
        @Override
        public Integer call(Integer i1, Integer i2) {
          return i1 + i2;
        }
      });

    wordCounts.print();
    jssc.start();
    jssc.awaitTermination();
  }
}
```

## [Scala](http://hortonworks.com/hadoop-tutorial/using-apache-spark-technical-preview-with-hdp-2-2/)

```scala
val file = sc.textFile("/tmp/data")
val counts = file.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey(_ + _)
counts.saveAsTextFile("/tmp/wordcount")
```

## [Scala + Kafka (streaming)](https://github.com/apache/spark/blob/master/examples/src/main/scala/org/apache/spark/examples/streaming/KafkaWordCount.scala)

```scala
package org.apache.spark.examples.streaming

import java.util.HashMap

import org.apache.kafka.clients.producer.{ProducerConfig, KafkaProducer, ProducerRecord}

import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf

object KafkaWordCount {
  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: KafkaWordCount <zkQuorum> <group> <topics> <numThreads>")
      System.exit(1)
    }

    StreamingExamples.setStreamingLogLevels()

    val Array(zkQuorum, group, topics, numThreads) = args
    val sparkConf = new SparkConf().setAppName("KafkaWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    ssc.checkpoint("checkpoint")

    val topicMap = topics.split(",").map((_, numThreads.toInt)).toMap
    val lines = KafkaUtils.createStream(ssc, zkQuorum, group, topicMap).map(_._2)
    val words = lines.flatMap(_.split(" "))
    val wordCounts = words.map(x => (x, 1L)).reduceByKeyAndWindow(_ + _, _ - _, Minutes(10), Seconds(2), 2)
    wordCounts.print()

    ssc.start()
    ssc.awaitTermination()
  }
}
```

## [Scala (Spark + Akka)](https://github.com/apache/spark/blob/master/examples/src/main/scala/org/apache/spark/examples/streaming/ActorWordCount.scala)

```scala
package org.apache.spark.examples.streaming

import scala.collection.mutable.LinkedList
import scala.reflect.ClassTag
import scala.util.Random

import akka.actor.{Actor, ActorRef, Props, actorRef2Scala}

import org.apache.spark.{SparkConf, SecurityManager}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.StreamingContext.toPairDStreamFunctions
import org.apache.spark.util.AkkaUtils
import org.apache.spark.streaming.receiver.ActorHelper

case class SubscribeReceiver(receiverActor: ActorRef)
case class UnsubscribeReceiver(receiverActor: ActorRef)

class FeederActor extends Actor {

  val rand = new Random()
  var receivers: LinkedList[ActorRef] = new LinkedList[ActorRef]()

  val strings: Array[String] = Array("words ", "may ", "count ")

  def makeMessage(): String = {
    val x = rand.nextInt(3)
    strings(x) + strings(2 - x)
  }

  new Thread() {
    override def run() {
      while (true) {
        Thread.sleep(500)
        receivers.foreach(_ ! makeMessage)
      }
    }
  }.start()

  def receive: Receive = {

    case SubscribeReceiver(receiverActor: ActorRef) =>
      println("received subscribe from %s".format(receiverActor.toString))
    receivers = LinkedList(receiverActor) ++ receivers

    case UnsubscribeReceiver(receiverActor: ActorRef) =>
      println("received unsubscribe from %s".format(receiverActor.toString))
    receivers = receivers.dropWhile(x => x eq receiverActor)

  }
}

class SampleActorReceiver[T: ClassTag](urlOfPublisher: String)
extends Actor with ActorHelper {

  lazy private val remotePublisher = context.actorSelection(urlOfPublisher)

  override def preStart(): Unit = remotePublisher ! SubscribeReceiver(context.self)

  def receive: PartialFunction[Any, Unit] = {
    case msg => store(msg.asInstanceOf[T])
  }

  override def postStop(): Unit = remotePublisher ! UnsubscribeReceiver(context.self)
}

object FeederActor {

  def main(args: Array[String]) {
    if (args.length < 2){
      System.err.println("Usage: FeederActor <hostname> <port>\n")
      System.exit(1)
    }
    val Seq(host, port) = args.toSeq

    val conf = new SparkConf
    val actorSystem = AkkaUtils.createActorSystem("test", host, port.toInt, conf = conf,
      securityManager = new SecurityManager(conf))._1
    val feeder = actorSystem.actorOf(Props[FeederActor], "FeederActor")

    println("Feeder started as:" + feeder)

    actorSystem.awaitTermination()
  }
}

object ActorWordCount {
  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println(
        "Usage: ActorWordCount <hostname> <port>")
      System.exit(1)
    }

    StreamingExamples.setStreamingLogLevels()

    val Seq(host, port) = args.toSeq
    val sparkConf = new SparkConf().setAppName("ActorWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(2))

    val lines = ssc.actorStream[String](
      Props(new SampleActorReceiver[String]("akka.tcp://test@%s:%s/user/FeederActor".format(host, port.toInt))), "SampleReceiver")

    lines.flatMap(_.split("\\s+")).map(x => (x, 1)).reduceByKey(_ + _).print()

    ssc.start()
    ssc.awaitTermination()
  }
}
```

# Akka (streaming)

## [Scala](https://github.com/pkinsky/akka-streams-example/blob/master/src/main/scala/WordCount.scala)

```scala
import akka.actor.ActorSystem
import akka.stream.scaladsl._
import akka.stream._
import scala.language.postfixOps
import scala.concurrent.duration._
import scala.concurrent.Future


object Main {
  implicit val as = ActorSystem()
  implicit val ec = as.dispatcher
  val settings = ActorFlowMaterializerSettings(as)
  implicit val mat = ActorFlowMaterializer(settings)

  val redditAPIRate = 500 millis
  def throttle[T](rate: FiniteDuration): Flow[T, T, Unit] = {
    Flow() { implicit builder =>
      import akka.stream.scaladsl.FlowGraph.Implicits._
      val zip = builder.add(Zip[T, Unit.type]())
      Source(rate, rate, Unit) ~> zip.in1
      (zip.in0, zip.out)
    }.map(_._1)
  }

  val fetchLinks: Flow[String, Link, Unit] =
    Flow[String]
        .via(throttle(redditAPIRate))
        .mapAsyncUnordered( subreddit => RedditAPI.popularLinks(subreddit) )
        .mapConcat( listing => listing.links )


  val fetchComments: Flow[Link, Comment, Unit] =
    Flow[Link]
        .via(throttle(redditAPIRate))
        .mapAsyncUnordered( link => RedditAPI.popularComments(link) )
        .mapConcat( listing => listing.comments )

  val wordCountSink: Sink[Comment, Future[Map[String, WordCount]]] =
    Sink.fold(Map.empty[String, WordCount])(
      (acc: Map[String, WordCount], c: Comment) =>
        mergeWordCounts(acc, Map(c.subreddit -> c.toWordCount))
    )

def main(args: Array[String]): Unit = {
    val subreddits: Source[String, Unit] =
      if (args.isEmpty)
        Source(RedditAPI.popularSubreddits).mapConcat(identity)
      else
        Source(args.toVector)

    val res: Future[Map[String, WordCount]] =
      subreddits
      .via(fetchLinks)
      .via(fetchComments)
      .runWith(wordCountSink)

    res.onComplete(writeResults)

    as.awaitTermination()
  }
}
```

# python-rq (streaming)

## [Python](http://python-rq.org/docs/)

```python
import requests

def count_words_at_url(url):
    resp = requests.get(url)
    return len(resp.text.split())

from rq import Queue
from redis import Redis
from somewhere import count_words_at_url

# Tell RQ what Redis connection to use
redis_conn = Redis()
q = Queue(connection=redis_conn)  # no args implies the default queue

# Delay execution of count_words_at_url('http://nvie.com')
job = q.enqueue(count_words_at_url, 'http://nvie.com')
print job.result   # => None

# Now, wait a while, until the worker is finished
time.sleep(2)
print job.result   # => 889
```

# raw (batch)

## [Scala](https://github.com/twitter/summingbird)

```scala
def wordCount(source: Iterable[String], store: MutableMap[String, Long]) =
  source.flatMap { sentence =>
    toWords(sentence).map(_ -> 1L)
  }.foreach { case (k, v) => store.update(k, store.get(k) + v) }
```

# Hailstorm (streaming)

## [Haskell](https://github.com/hailstorm-hs/hailstorm/blob/3c595336038a7a6417a31cb8b1fc38780f27c520/src/Hailstorm/Sample/WordCountSample.hs)

```haskell
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveDataTypeable #-}
module Hailstorm.Sample.WordCountSample
( wordCountTopology
) where

import Control.Exception
import Control.Monad
import Data.Dynamic
import Data.Hashable
import Data.List
import Data.Maybe
import Data.Monoid
import Data.Ord
import Hailstorm.Error
import Hailstorm.Topology.HardcodedTopology
import Hailstorm.Processor
import Hailstorm.TransactionTypes
import System.IO
import Pipes
import qualified Data.ByteString.Char8 as C8
import qualified Data.Map as Map
import qualified Data.PSQueue as PS

localServer :: String
localServer = "127.0.0.1"

outputFilename :: String
outputFilename = "top_words.txt"

forceDyn :: Typeable a => Dynamic -> a
forceDyn x = case fromDynamic x of
  Just y -> y
  Nothing -> throw $ InvalidTopologyError $
             "Word count sample forced dynamic to bad type: " ++ show x

deriving instance Typeable2 PS.PSQ

data TypeableIntSum = TIS (Sum Int)
                      deriving (Eq, Typeable, Show, Read)

instance Ord TypeableIntSum where
   (TIS (Sum a)) `compare` (TIS (Sum b)) = a `compare` b

instance Monoid TypeableIntSum where
    mempty = TIS (mempty :: (Sum Int))
    (TIS x) `mappend` (TIS y) = TIS (x `mappend` y)


data MonoidMapWrapper k v = (Ord k, Monoid v) => MonoidMapWrapper (Map.Map k v)
                            deriving (Typeable)
instance (Ord k, Monoid v) => Monoid (MonoidMapWrapper k v) where
    mempty = MonoidMapWrapper Map.empty
    (MonoidMapWrapper m) `mappend` (MonoidMapWrapper n) = MonoidMapWrapper $
        Map.unionWith mappend m n

dynToWordCountTuple :: Dynamic -> (String, TypeableIntSum)
dynToWordCountTuple d = flip fromMaybe (fromDynamic d) $
    throw $ InvalidTopologyError "Unexpected value: not a (word, count) tuple"


dynToMMWrapper :: Dynamic -> MonoidMapWrapper String TypeableIntSum
dynToMMWrapper d = flip fromMaybe (fromDynamic d) $
    throw $ InvalidTopologyError "Unexpected value: not a word-count) state map"

payloadTupleToWordCountTuple :: PayloadTuple -> (String, TypeableIntSum)
payloadTupleToWordCountTuple (MkPayloadTuple d) = dynToWordCountTuple d

readTISPayloadTuple :: String -> PayloadTuple
readTISPayloadTuple x = MkPayloadTuple $ toDyn (read x :: (String, TypeableIntSum))

wordsSpout :: Spout
wordsSpout = Spout
    { spoutName = "words"
    , spoutPartitions = ["all_words"]
    , convertFn = \x ->
        MkPayloadTuple $ toDyn (C8.unpack x, TIS (Sum 1))
    , spoutSerializer = show . payloadTupleToWordCountTuple
    }

countBolt :: Bolt
countBolt = Bolt
    { boltName = "count"

    , boltParallelism = 2

    , upstreamDeserializer = readTISPayloadTuple

    , transformTupleFn = \tup (MkBoltState dynM) ->
        let MonoidMapWrapper m = dynToMMWrapper dynM
            (k, _) = payloadTupleToWordCountTuple tup
            v = Map.findWithDefault
                (error $ "Could not find " ++ k ++ " in state " ++ show m) k m
        in (MkPayloadTuple $ toDyn (k, v))

    , emptyState =
        MkBoltState $ toDyn (mempty :: MonoidMapWrapper String TypeableIntSum)

    , mergeFn = \(MkBoltState dynM) (MkBoltState dynN) ->
        let m' = dynToMMWrapper dynM
            n' = dynToMMWrapper dynN
            mr = m' `mappend` n'
        in MkBoltState $ toDyn mr

    , tupleToStateConverter = \tup ->
        let (key, val) = payloadTupleToWordCountTuple tup
        in MkBoltState $ toDyn $ MonoidMapWrapper (Map.singleton key val)

    , downstreamSerializer = show . payloadTupleToWordCountTuple

    , stateDeserializer = \x -> MkBoltState $ toDyn $
        MonoidMapWrapper (read x :: Map.Map String TypeableIntSum)

    , stateSerializer = \(MkBoltState d) -> show $
        let (MonoidMapWrapper m') = dynToMMWrapper d in m'
    }



mergePSTopN :: Int -> PS.PSQ String Int -> PS.PSQ String Int -> PS.PSQ String Int
mergePSTopN n p1 p2 =
  let (lesser, greater) = if PS.size p1 < PS.size p2 then (p1,p2) else (p2, p1)
      merged = foldr (\(k PS.:-> v) pq ->
                        PS.alter (maybeMax v) k pq
                     ) greater (PS.toList lesser)
  in purgeN n merged
  where
    maybeMax a (Just b) = Just $ max a b
    maybeMax a Nothing = Just $ a
    purgeN np pq = if (PS.size pq) <= n || np <= 0 then pq
                   else purgeN (np-1) (PS.deleteMin pq)


amtToSelect :: Int
amtToSelect = 20

topNBolt :: Bolt
topNBolt = Bolt
    { boltName = "topn"

    , boltParallelism = 2

    , upstreamDeserializer = readTISPayloadTuple

    , transformTupleFn = \_ (MkBoltState stateDyn) ->
        let state = forceDyn $ stateDyn :: PS.PSQ String Int
        in MkPayloadTuple $ toDyn $ map (\(k PS.:-> v) -> (k,v)) (PS.toList state)

    , emptyState =
        MkBoltState $ toDyn (PS.empty :: PS.PSQ String Int)

    , mergeFn = \(MkBoltState ps1Dyn) (MkBoltState ps2Dyn) ->
        let ps1 = forceDyn $ ps1Dyn :: PS.PSQ String Int
            ps2 = forceDyn $ ps2Dyn :: PS.PSQ String Int
        in MkBoltState $ toDyn $ mergePSTopN amtToSelect ps1 ps2

    , tupleToStateConverter = \tup ->
        let (key, TIS (Sum i)) = payloadTupleToWordCountTuple tup
        in MkBoltState $ toDyn $ PS.singleton key i

    , downstreamSerializer = \(MkPayloadTuple d) ->
        show $ (forceDyn d :: [(String, Int)])

    , stateDeserializer = \str ->
        let pq = PS.fromList $ map (\(k,v) -> k PS.:-> v) $ (read str :: [(String, Int)])
        in MkBoltState $ toDyn $ pq

    , stateSerializer = \(MkBoltState psDyn) ->
        show $ map (\(k PS.:-> v) -> (k,v)) $ PS.toList $ (forceDyn psDyn :: PS.PSQ String Int)
    }

outputAmt :: Int
outputAmt = 20

mergeSortBolt:: Bolt
mergeSortBolt = Bolt
    { boltName = "merge_sort"

    , boltParallelism = 1

    , upstreamDeserializer = \str ->
        MkPayloadTuple $ toDyn (read str :: [(String, Int)])

    , transformTupleFn = \_ (MkBoltState stateDyn) ->
        let state = forceDyn $ stateDyn :: PS.PSQ String Int
        in MkPayloadTuple $ toDyn $ sortBy (flip $ comparing $ snd) $
              map (\(k PS.:-> v) -> (k,v)) (PS.toList state)

    , emptyState =
        MkBoltState $ toDyn (PS.empty :: PS.PSQ String Int)

    , mergeFn = \(MkBoltState ps1Dyn) (MkBoltState ps2Dyn) ->
        let ps1 = forceDyn $ ps1Dyn :: PS.PSQ String Int
            ps2 = forceDyn $ ps2Dyn :: PS.PSQ String Int
        in MkBoltState $ toDyn $ mergePSTopN outputAmt ps1 ps2

    , tupleToStateConverter = \(MkPayloadTuple tup) ->
        let ls = map (\(k,v) -> k PS.:-> v) (forceDyn tup :: [(String, Int)])
        in MkBoltState $ toDyn $ PS.fromList ls

    , downstreamSerializer = \(MkPayloadTuple d) ->
        show $ (forceDyn d :: [(String, Int)])

    , stateDeserializer = \str ->
        let pq = PS.fromList $ map (\(k,v) -> k PS.:-> v) $ (read str :: [(String, Int)])
        in MkBoltState $ toDyn $ pq

    , stateSerializer = \(MkBoltState psDyn) ->
        show $ map (\(k PS.:-> v) -> (k,v)) $ PS.toList $ (forceDyn psDyn :: PS.PSQ String Int)
    }


printSorted :: Int -> Consumer PayloadTuple IO ()
printSorted cnt = do
  (MkPayloadTuple x) <- await
  lift $ when (cnt `mod` 500 == 0) $ do
      let wordsAndCounts = forceDyn x :: [(String, Int)]
          outputWC h (w, c) = hPutStrLn h $ w ++ "," ++ show c
      h <- openFile outputFilename WriteMode
      mapM_ (outputWC h) wordsAndCounts
      hClose h
  printSorted (cnt + 1)

outputSink :: Sink
outputSink = Sink
    { sinkName = "sink"
    , sinkParallelism = 1
    , outputConsumer = printSorted 0
    , sinkDeserializer = \str ->
        MkPayloadTuple $ toDyn (read str:: [(String, Int)])
    }


wordCountTopology :: HardcodedTopology
wordCountTopology = HardcodedTopology
  {
      processorNodeMap = mkProcessorMap
      [ SpoutNode wordsSpout
      , BoltNode countBolt
      , BoltNode topNBolt
      , BoltNode mergeSortBolt
      , SinkNode outputSink
      ]
      ,
      downstreamMap = Map.fromList
      [ ("words", [("count", \(MkPayloadTuple dyn) ->
                      hash $ fst $ (forceDyn dyn :: (String, TypeableIntSum)))])
      , ("count", [("topn", \(MkPayloadTuple dyn) ->
                      hash $ fst $ (forceDyn dyn :: (String, TypeableIntSum)))])
      , ("topn", [("merge_sort", const 0)])
      , ("merge_sort", [("sink", const 0)])
      ]
      ,
      addresses = Map.fromList
      [ (("sink", 0), (localServer, "10000"))
      , (("count", 0), (localServer, "10001"))
      , (("count", 1), (localServer, "10004"))
      , (("topn", 0), (localServer, "10002"))
      , (("topn", 1), (localServer, "10005"))
      , (("merge_sort", 0), (localServer, "10003"))
      ]
  }
```
