sudo rabbitmqctl add_user myuser mypassword
sudo rabbitmqctl add_vhost myvhost
sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"

sudo rabbitmq-server
sudo rabbitmq-server -detached
sudo rabbitmqctl stop

sudo rabbitmqctl list_queues name messages_ready messages_unacknowledged
sudo rabbitmqctl list_exchanges
rabbitmqctl list_bindings

rabbitmq-plugins list
sudo rabbitmq-plugins enable rabbitmq_management
curl http://localhost:15672/

exchange types:
* default (''): direct; all queues bound to it with their name as routing key.
* direct: queues bound by routing keys.
* fanout: send to all bound queues. useful to get messages to all subscribers, rest for distributing work.
* topic: queues bound by fuzzy routing key patterns.
* headers: queues bound by k/v attributes.

'fair dispatch' on consumers: `channel.basic_qos(prefetch_count=n)`

[shovels](https://www.rabbitmq.com/shovel.html): redirect messages
