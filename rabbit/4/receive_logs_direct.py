#!/usr/bin/env python
import pika
import sys

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.exchange_declare(exchange='direct_logs', type='direct')

def callback(ch, method, properties, body):
    print " [x] %r:%r" % (method.routing_key, body,)

queue_name = channel.queue_declare(exclusive=True).method.queue
severities = sys.argv[1:]
if not severities: sys.exit("Usage: %s [info] [warning] [error]" % (sys.argv[0],))

for severity in severities:
    channel.queue_bind(exchange='direct_logs', queue=queue_name, routing_key=severity)

print ' [*] Waiting for logs. To exit press CTRL+C'
channel.basic_consume(callback, queue=queue_name, no_ack=True)
channel.start_consuming()
