#!/usr/bin/env python
import pika
import sys

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.exchange_declare(exchange='topic_logs', type='topic')

def callback(ch, method, properties, body):
    print " [x] %r:%r" % (method.routing_key, body,)

queue_name = channel.queue_declare(exclusive=True).method.queue
binding_keys = sys.argv[1:]
if not binding_keys: sys.exit("Usage: %s [binding_key]..." % (sys.argv[0],))

for binding_key in binding_keys:
    channel.queue_bind(exchange='topic_logs', queue=queue_name, routing_key=binding_key)

print ' [*] Waiting for logs. To exit press CTRL+C'
channel.basic_consume(callback, queue=queue_name, no_ack=True)
channel.start_consuming()
